# Trial GUIs for IE, MT, ASR and Text Classification

This repository contains the standard ELG trial GUIs for most of the main service types except text-to-speech and dependency parser services.  The principal entry points are the files in the top-level `html` directory.  To understand how to configure the UIs you may wish to refer to the documentation of the JSON formats [in the ELG user guide](https://european-language-grid.readthedocs.io/en/stable/all/A3_API/LTInternalAPI.html#response-structure).

## For services that process text

- `index.html` - for services that take text as input and return either "texts" or "annotations" responses.  In the case of an annotations response the result viewer shows the original input text with any annotations highlighted, and response-level features below.  In the case of a texts response the response text is rendered appropriately for the "role" of each item in the texts tree.
  - for a list of `alternative` nodes just one option is shown at a time with buttons to switch between them
  - a list of `token` or `word` nodes is treated like a segment made up of the "content" of the individual words, concatenated with spaces and highlighted with synthesized annotations
  - any other role (such as "segment", "sentence" or "paragraph") is rendered in a box labelled with the role name.
- `index-mt.html` - handles the same text input and texts/annotations output as `index.html`, but the result viewer shows the original text submitted by the user along with the rendered response.  This is most suitable for services such as Machine Translation, where you want to see the original text and the translation together.
- `index-text-classification.html` - for services that return the "classification" response type.
- `index-dependency.html` - for dependency parsers that return annotations or texts responses representing a dependency tree.  This GUI has many options, described on [its own page](dependency-parser.md)
- `index-qa-in-context.html` - originally designed as a special-purpose GUI for services that do question answering from a context string, this GUI can be used for any service that expects _two_ texts as its input rather than one.  It is described in full on [its own page](qa-in-context.md)

## For services that process audio

- `index-asr.html` - for services that take audio and return "texts", typically Automatic Speech Recognition.  The response is rendered exactly as for `index-mt.html`
- `index-audio-annotation.html` - for services that take audio and return "annotations" whose `start` and `end` are floating point numbers counting in seconds from the start of the audio stream.  The annotations are rendered as coloured highlights on a waveform representation of the audio timeline.

## For services that process images

- `index-image.html` - services that take images as input and return "texts" as output (e.g. image OCR).  Renders the response the same way as for MT/ASR, but also shows the original image above and highlights any bounding boxes found in the "features" of the response.

## Configuration

Many aspects of the GUIs will adapt themselves automatically based on the JSON metadata of the ELG service to which they are being applied.  For example `index-mt.html` by default will label the text input field "Type text to translate", but if the service metadata indicates that it is being used for a summarization service this will automatically change to "Type text to summarize".  Any parameters declared in the service metadata will be made available as form fields below the main text or audio input controls, and any input samples will be shown as clickable buttons that populate the input controls from that item of sample data.

In addition there are a number of configuration options that can be specified as URL query parameters when a validator assigns the "try out" GUI URL in the ELG platform.

- Text direction: specify `?dir=ltr` or `?dir=rtl` to define the predominant directionality of the HTML elements that render the returned text.  In the case of `index-mt.html` there are separate parameters for `srcdir` (the directionality of the input/source language) and `targetdir` (the output/target language).
  - It _is_ worth setting an explicit direction even for left-to-right languages, so the GUI behaves properly even when rendered in a browser that defaults to right-to-left
- Default "role" values: for a "texts" response, the API allows the service to declare the "role" of each node, for example a two-level texts response might represent a list of segments where each segment has a list of alternative transcriptions, or it might be a list of alternatives where each alternative is a list of segments.  If the service does not specify an explicit role for each node then this GUI will not know how to render the result.  To overcome this, specify `?roles=level1-level2-...` to define the _default_ role at each level of the hierarchy (for nodes that do not specify one explicitly).  For example `?roles=segment-alternative` or `?roles=alternative-segment`
  - The role "token" or "word" is handled specially, as described above a node containing a list of texts with either of these roles is treated as a single segment constructed from the individual texts concatenated with spaces and marked with synthesized "token"/"word" annotations.  Any annotations at the parent node level (whose start and end are indexes into the list of words) will be rendered as if they span the synthesized tokens.  Thus for a tokeniser service that returns its response as a list of sentences each holding a list of tokens, specify `?roles=sentence-token` to render the result in a readable format.
- Showing/hiding annotations: annotations (including synthetic token/word annotations) are rendered as coloured highlights over the text or audio, and all annotation types are highlighted by default.  If this is not desirable you can specify any number of `show=...` and `hide=...` parameters to change the default visibility.  If there are any `show=type` parameters then only those types are selected initially.  If there are any `hide=type` parameters then those types are _deselected_ initially.  All types remain available for the user to view if they wish, these options just control which checkboxes are selected initially when the response is shown.
- Audio format for microphone: the audio input UIs provide the option to record audio live into the browser using a microphone.  By default the recorded audio is encoded as stereo MP3 at the default sample rate of the audio device, if a service requires a different format, channel count or sample rate this can be changed with parameters.  For example, 16kHz mono WAV is `?audioFormat=wav&audioChannels=1&audioRate=16000`

# Repository structure

Most of the content is under the "html" folder.  The top level JavaScript file for each GUI type loads other modules using [RequireJS](https://requirejs.org).  These modules can be found in the `assets` folder.

Apart from the third party libraries, most of the rest of the code is TypeScript.  The module `assets/annotation-viewer` compiles into a single `bundle.js` file, the other TypeScript files in `assets/elg` compile into individual `.js` files.  This is all orchestrated by the top-level [`package.json`](package.json); to install the dependencies and TypeScript `.d.ts` type definitions for third party modules, use

```
npm install
```

To compile the TypeScript modules to plain JavaScript use

```
npm run build
```

This is only required for development purposes - for the production builds the TypeScript compilation happens as part of the image build process as defined in the [Dockerfile](Dockerfile).  You can build a local Docker image (e.g. for testing as part of a [local-installation](https://european-language-grid.readthedocs.io/en/stable/all/A1_PythonSDK/DeployServicesLocally.html)) using

```
docker build -t gui-ie:latest .
```

## Sample helper

The `sample-helper.html` file is not part of the final `gui-ie` build itself, it is part of the mechanism to allow the GUIs to download sample data that is hosted on other web servers that cannot be configured to permit cross-origin requests from the ELG platform.  For these cases, a suitably-configured copy of `sample-helper.html` should be uploaded to the relevant server, and configuration supplied to the GUI via [sample-helpers.json](html/config/sample-helpers.json) (typically this file will be customized for each installation of the ELG platform and injected as a Kubernetes ConfigMap).  The configuration file causes the GUIs to load each sample helper as an invisible `iframe`, then when the GUI needs to download a sample from that web server it dispatches a message to the helper `iframe`, the helper does the download and returns the data in a reply message.  This way the actual download is performed by a JavaScript context that is on the _same origin_ as the target file, so CORS restrictions do not apply.

# Licences

This project includes the following dependencies:

- [jQuery](https://jquery.com) 2.2.0
- [Bootstrap](https://getbootstrap.com/docs/3.3/) 3.3.6
- [Underscore.js](https://underscorejs.org) 1.8.3
- [RequireJS](https://requirejs.org) 2.3.2
- [Material Components for the Web](https://github.com/material-components/material-components-web) 5.1.0

all of which are released under the MIT licence.  It also incorporates [WebAudioRecorder.js](https://github.com/higuma/web-audio-recorder-js) to allow live recording of audio for speech recognition, this library is under the MIT licence itself but it incorporates the LAME MP3 encoder which is released under the LGPL.  The audio waveform renderer used for "audio annotations" services is [WaveSurfer](https://wavesurfer-js.org) 5.2.0, which is under the BSD licence, slightly modified to make it work with requirejs.

The code for this project itself is released under the Apache Licence version 2.
