// fix up directionality for the result panel, e.g. Arabic should be rendered RTL
(function() {
    var myUrl = new URL(window.location.href);
    if(myUrl.searchParams.has('dir')) {
        document.getElementById('alternatives').dir = myUrl.searchParams.get('dir');
    }
})();

require(["jquery", "material-components-web", "elg/common", "elg/image-annotations", "elg/image-input", "elg/binary-samples"], function ($, mdc, ElgCommon, imageAnnotations, imageInput, binarySamples) {
    $(function () {
        mdc.autoInit();

        function enableSelect() {
            $('#selectfile').prop('disabled', false);
        }

        function enableSubmit() {
            $('#submit-form').prop('disabled', false);
        }

        var elgCommon = new ElgCommon(enableSelect, enableSubmit,
            document.getElementById('submitprogress').MDCLinearProgress,
            binarySamples($('#samples-list'), $('#samples-container'), "image", imageInput.imageReady));

        var handleResponse = imageAnnotations.responseHandler(elgCommon, "No text found in this image");

        imageInput.setup(elgCommon, handleResponse);
    });
});


