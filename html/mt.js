// fix up directionality for the test form and annotation result section, e.g. if this
// gui is being used for Arabic NER then the form and the result should be rendered as RTL
var elgNoResultMessage = "No translation available";
(function() {
    var myUrl = new URL(window.location.href);
    if(myUrl.searchParams.has('srcdir')) {
        document.getElementById('elg-test-form').dir = myUrl.searchParams.get('srcdir');
        document.getElementById('result-original').dir = myUrl.searchParams.get('srcdir');
    }
    if(myUrl.searchParams.has('targetdir')) {
        document.getElementById('alternatives').dir = myUrl.searchParams.get('targetdir');
    }
    // allow override of the "no result" message
    if(myUrl.searchParams.has('noresult')) {
        elgNoResultMessage = myUrl.searchParams.get('noresult');
    }
})();

require(["jquery", "material-components-web", "elg/common", "elg/text-alternatives", "elg/text-samples"], function ($, mdc, ElgCommon, textAlternatives, textSamples) {
    $(function () {
        mdc.autoInit();

        function enableSubmit() {
            $('#submit-form').prop('disabled', false);
        }

        var elgCommon = new ElgCommon(enableSubmit, enableSubmit,
            document.getElementById('submitprogress').MDCLinearProgress,
            textSamples($('#text-to-annotate'), $('#result-label'), $('#samples-list'), $('#samples-container, #test-form-header')));

        var text = '';

        var handleResponse = textAlternatives.responseHandler(elgCommon, elgNoResultMessage);

        $("#submit-form").on('click', function (e) {
            e.preventDefault();

            text = $("#text-to-annotate").val();
            ElgCommon.setTextWithLineBreaks($('#result-original'), text);

            // disable the button until the REST call returns
            $('#submit-form').prop('disabled', true);
            $('#elg-messages').empty();

            elgCommon.callService(text, "text/plain", handleResponse);
            return false;
        });
    });
});

