/* Extract common parameters from the URL */
var elgDefaultRoles;
var elgDefaultShownTypes;
var elgDefaultHiddenTypes;
var elgAudioFormat;
var elgAudioChannels;
var elgAudioSampleRate;
(function() {
    var myUrl = new URL(window.location.href);
    // extract default roles for each response level, if specified
    if(myUrl.searchParams.has('roles')) {
        elgDefaultRoles = myUrl.searchParams.get('roles').split('-');
    }
    // extract default shown/hidden annotation types, if specified
    if(myUrl.searchParams.has('show')) {
        elgDefaultShownTypes = myUrl.searchParams.getAll('show');
    }
    if(myUrl.searchParams.has('hide')) {
        elgDefaultHiddenTypes = myUrl.searchParams.getAll('hide');
    }
    // extract audio format, if specified
    if(myUrl.searchParams.has('audioFormat')) {
        elgAudioFormat = myUrl.searchParams.get('audioFormat');
    }
    if(myUrl.searchParams.has('audioChannels')) {
        elgAudioChannels = parseInt(myUrl.searchParams.get('audioChannels'));
    }
    if(myUrl.searchParams.has('audioRate')) {
        elgAudioSampleRate = parseInt(myUrl.searchParams.get('audioRate'));
    }
})();