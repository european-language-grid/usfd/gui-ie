// fix up directionality for the test form and annotation result section, e.g. if this
// gui is being used for Arabic NER then the form and the result should be rendered as RTL

// format can be structured or param.  If structured contextPath and questionPath should
// be numbers giving the relative order of the context and question in the structuredText
// texts array (context=0&question=1 or question=0&context=1).  If "param" then contextPath
// or questionPath should be the name of the parameter that is used to transmit that
// item - whichever one is not set will be used as the text of the request.
var format = "structured";
var contextPath;
var questionPath;
var contextName;
var contextDescription;
var questionName;
var questionDescription;
(function() {
    var myUrl = new URL(window.location.href);
    if(myUrl.searchParams.has('dir')) {
        document.getElementById('elg-test-form').dir = myUrl.searchParams.get('dir');
        document.getElementById('elg-annotate-result').dir = myUrl.searchParams.get('dir');
    }
    if(myUrl.searchParams.has('format')) {
        format = myUrl.searchParams.get('format');
    }
    if(myUrl.searchParams.has('context')) {
        contextPath = myUrl.searchParams.get('context');
    }
    if(myUrl.searchParams.has('context-name')) {
        contextName = myUrl.searchParams.get('context-name');
    }
    if(myUrl.searchParams.has('context-desc')) {
        contextDescription = myUrl.searchParams.get('context-desc');
    }
    if(myUrl.searchParams.has('question')) {
        questionPath = myUrl.searchParams.get('question');
    }
    if(myUrl.searchParams.has('question-name')) {
        questionName = myUrl.searchParams.get('question-name');
    }
    if(myUrl.searchParams.has('question-desc')) {
        questionDescription = myUrl.searchParams.get('question-desc');
    }
})();

require(["jquery", "material-components-web", "elg/common", "elg/text-samples", "elg/annotations-or-texts", "elg/classification-response"], function ($, mdc, ElgCommon, textSamples, annsOrTexts, classificationResponse) {
    $(function () {
        mdc.autoInit();

        function enableSubmit() {
            $('#submit-form').prop('disabled', false);
        }

        function contextAndQuestion(sampleText) {
            var request = JSON.parse(sampleText);
            var context, question;
            if(format === 'structured') {
                if((''+(contextPath || '0')) <= (''+(questionPath || '0'))) {
                    context = request.texts[0].content;
                    question = request.texts[1].content;
                } else {
                    context = request.texts[1].content;
                    question = request.texts[0].content;
                }
            } else if(format === 'param') {
                if(typeof contextPath === 'undefined') {
                    // context as text, question as param
                    context = request.content;
                    question = request.params[questionPath];
                } else {
                    // question as text, context as param
                    question = request.content;
                    context = request.params[contextPath];
                }
            }
            if(context && question) {
                return [context, question];
            } else {
                return null;
            }
        }

        function formatSample(sample, elt) {
            var cq = contextAndQuestion(sample.sample_text);
            if(cq) {
                var context = cq[0];
                var question = cq[1];
                elt.empty();
                $("<em>" + contextName + ": </em>").appendTo(elt);
                $("<span></span>").text(context).appendTo(elt);
                $("<br>").appendTo(elt);
                $("<em>" + questionName + ": </em>").appendTo(elt);
                $("<span></span>").text(question).appendTo(elt);
            } else {
                elt.html("<em>Malformed sample</em>");
            }
        }

        function insertSample(sample) {
            var context = '', question = '';
            var cq = contextAndQuestion(sample.sample_text);
            if (cq) {
                context = cq[0];
                question = cq[1];
            }
            ($('#context-text').closest('.mdc-text-field')[0]).MDCTextField.value = context;
            ($('#question-text').closest('.mdc-text-field')[0]).MDCTextField.value = question;
        }

        var samplesHandler = textSamples($('#context-text, #question-text'), $(), $('#samples-list'),
            $('#samples-container, #test-form-header'), formatSample, insertSample);

        function metadataHandler(metadata, elgCommon) {
            samplesHandler(metadata, elgCommon);

            // fix up any overridden labels - this has to happen after the
            // samplesHandler as it sometimes changes the labels depending on
            // the metadata classification.
            if(contextName) {
                // "Context" name overridden, so fix up field placeholder
                $('#context-text-label').text(contextName);
            } else {
                // take default name from HTML
                contextName = $('#context-text-label').text();
            }

            if(typeof contextDescription !== 'undefined') {
                // "Context" help text overridden
                $('#context-helper').text(contextDescription);
            }

            if(questionName) {
                // "Question" name overridden, so fix up field placeholder
                $('#question-text-label').text(questionName);
            } else {
                // take default name from HTML
                questionName = $('#question-text-label').text();
            }

            if(typeof questionDescription !== 'undefined') {
                // "Question" help text overridden
                $('#question-helper').text(questionDescription);
            }
        }

        var elgCommon = new ElgCommon(enableSubmit, enableSubmit,
            document.getElementById('submitprogress').MDCLinearProgress,
            metadataHandler);

        $('#test-again').on('click', function(e) {
            e.preventDefault();
            $('#elg-annotate-result').addClass('hidden');
            $('#elg-test-form').removeClass('hidden');
        });

        var handleAnnsOrTextsResponse = annsOrTexts.responseHandler(elgCommon);
        var handleClassificationResponse = classificationResponse.responseHandler(elgCommon);

        function handleResponse(data) {
            if (data.response && data.response.type == 'classification') {
                $('#classes-container').removeClass('hidden');
                $('#result-answer').addClass('hidden');
                handleClassificationResponse(data);
            } else {
                $('#classes-container').addClass('hidden');
                $('#result-answer').removeClass('hidden');
                handleAnnsOrTextsResponse(data);
            }
        }

        $("#submit-form").on('click', function (e) {
            e.preventDefault();
            var context = $("#context-text").val();
            annsOrTexts.useText(context);
            var question = $('#question-text').val();

            var request = {};
            if(format === 'structured') {
                request.type = 'structuredText';
                request.texts = [];
                if((''+(contextPath || '0')) <= (''+(questionPath || '0'))) {
                    request.texts.push({'content':context});
                    request.texts.push({'content':question});
                } else {
                    request.texts.push({'content':question});
                    request.texts.push({'content':context});
                }
                annsOrTexts.useTexts(request.texts);
            } else if(format === 'param') {
                request.type = 'text';
                request.params = {};
                if(typeof contextPath === 'undefined') {
                    // context as text, question as param
                    request.content = context;
                    request.params[questionPath] = question;
                } else {
                    // question as text, context as param
                    request.content = question;
                    request.params[contextPath] = context;
                }
                annsOrTexts.useText(request.content);
            }

            // disable the button until the REST call returns
            $('#submit-form').prop('disabled', true);
            $('#elg-messages').empty();

            elgCommon.callService(request, "application/json", handleResponse);
            return false;
        });
    });
});


