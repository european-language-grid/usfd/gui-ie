// fix up directionality for the result panel, e.g. Arabic should be rendered RTL
var elgNoResultMessage = "No transcription available";
(function() {
    var myUrl = new URL(window.location.href);
    if(myUrl.searchParams.has('dir')) {
        document.getElementById('alternatives').dir = myUrl.searchParams.get('dir');
    }
    // allow override of the "no result" message
    if(myUrl.searchParams.has('noresult')) {
        elgNoResultMessage = myUrl.searchParams.get('noresult');
    }
})();

require(["jquery", "material-components-web", "elg/common", "elg/text-alternatives", "elg/audio-input", "elg/binary-samples"], function ($, mdc, ElgCommon, textAlternatives, audioInput, binarySamples) {
    $(function () {
        mdc.autoInit();

        function enableRecord() {
            $('#recordmic, #selectfile').prop('disabled', false);
        }

        function enableSubmit() {
            $('#submit-form').prop('disabled', false);
        }

        var elgCommon = new ElgCommon(enableRecord, enableSubmit,
            document.getElementById('submitprogress').MDCLinearProgress,
            binarySamples($('#samples-list'), $('#samples-container'), "audio", audioInput.audioReady));

        var handleResponse = textAlternatives.responseHandler(elgCommon, elgNoResultMessage);

        audioInput.setup(elgCommon, handleResponse);
    });
});


