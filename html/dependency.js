// fix up directionality for the test form and annotation result section, e.g. if this
// gui is being used for Arabic then the form and the result sentence should be rendered as RTL
(function() {
    var myUrl = new URL(window.location.href);
    if(myUrl.searchParams.has('dir')) {
        var dir = myUrl.searchParams.get('dir');
        if(dir === 'ltr' || dir === 'rtl') {
            document.getElementById('elg-test-form').dir = dir;
            var css = document.createElement('style');
            css.textContent = "#treeView div[sentence] { direction: " + dir + "; }";
            document.head.appendChild(css);
        }
    }
})();

require(["jquery", "material-components-web", "elg/common", "elg/text-samples", "elg/annotations-or-texts", "elg/dependency-tree"], function ($, mdc, ElgCommon, textSamples, annsOrTexts, depTree) {
    $(function () {
        mdc.autoInit();

        function enableSubmit() {
            $('#submit-form').prop('disabled', false);
        }

        var elgCommon = new ElgCommon(enableSubmit, enableSubmit,
            document.getElementById('submitprogress').MDCLinearProgress,
            textSamples($('#text-to-annotate'), $(), $('#samples-list'), $('#samples-container, #test-form-header')));

        $('#test-again').on('click', function(e) {
            e.preventDefault();
            $('#elg-annotate-result').addClass('hidden');
            $('#elg-test-form').removeClass('hidden');
        });

        var handleResponse = depTree.responseHandler(elgCommon);

        $("#submit-form").on('click', function (e) {
            e.preventDefault();
            var text = $("#text-to-annotate").val();
            annsOrTexts.useText(text);

            // disable the button until the REST call returns
            $('#submit-form').prop('disabled', true);
            $('#elg-messages').empty();

            elgCommon.callService(text, "text/plain", handleResponse);
            return false;
        });
    });
});


