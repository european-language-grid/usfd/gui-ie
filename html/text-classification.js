// fix up directionality for the test form and annotation result section, e.g. if this
// gui is being used for Arabic classification then the form and the text in the result
// should be rendered as RTL
(function() {
    var myUrl = new URL(window.location.href);
    if(myUrl.searchParams.has('dir')) {
        document.getElementById('elg-test-form').dir = myUrl.searchParams.get('dir');
        document.getElementById('result-original').dir = myUrl.searchParams.get('dir');
    }
})();

require(["jquery", "material-components-web", "elg/common", "elg/text-samples", "elg/classification-response"], function ($, mdc, ElgCommon, textSamples, classificationResponse) {
    $(function () {
        mdc.autoInit();

        function enableSubmit() {
            $('#submit-form').prop('disabled', false);
        }

        var elgCommon = new ElgCommon(enableSubmit, enableSubmit,
            document.getElementById('submitprogress').MDCLinearProgress,
            textSamples($('#text-to-annotate'), $(), $('#samples-list'), $('#samples-container, #test-form-header')));

        $('#test-again').on('click', function(e) {
            e.preventDefault();
            $('#elg-annotate-result').addClass('hidden');
            $('#elg-test-form').removeClass('hidden');
        });

        var text = '';

        var handleResponse = classificationResponse.responseHandler(elgCommon);

        $("#submit-form").on('click', function (e) {
            e.preventDefault();

            text = $("#text-to-annotate").val();
            ElgCommon.setTextWithLineBreaks($('#result-original'), text);

            // disable the button until the REST call returns
            $('#submit-form').prop('disabled', true);
            $('#elg-messages').empty();

            elgCommon.callService(text, "text/plain", handleResponse);
            return false;
        });
    });
});


