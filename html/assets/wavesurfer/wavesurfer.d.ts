// Type definition assuming we have npm installed the right @types/wavesurfer.js version

import WaveSurfer from 'wavesurfer.js/src/wavesurfer';

export = WaveSurfer;
export as namespace WaveSurfer;