import * as $ from "jquery";
import sampleFetcher = require("./sample-fetcher");
import ElgCommon = require("./common");

type ReadyFunction = (blob: Blob, desc: string) => void;

/**
 * Returns a function which can be used as a metadata callback with ElgCommon.
 * The function will extract audio samples from the service metadata and
 * create a suitable user interface to allow the user to select one of the samples
 * and use its content for submission to the remote service.
 * @param target jQuery object referencing the container element (typically a div)
 * where the sample selection UI should be put.
 * @param toShow jQuery object referencing any hidden elements that should be shown
 * if there are any valid samples.
 * @param sampleType type of sample to pass to the sample fetcher.
 * @param ready function to call when the sample data has been retrieved and is ready to be processed.
 * @returns function suitable to be passed as the metadataCallback argument to
 * ElgCommon.
 */
function metadataHandler(target: JQuery, toShow: JQuery, sampleType: string, ready: ReadyFunction): (md: any, common: ElgCommon) => void {
    const msgsContainer = $('#elg-messages');

    function populateSamples(metadata: any, elgCommon: ElgCommon): void {
        const validSamples: ElgSample[] = [];
        // we want to extract all samples from all input resource types that are
        // either "user input text" or a "file" of the "text" media type.
        if (metadata.described_entity &&
            metadata.described_entity.lr_subclass &&
            metadata.described_entity.lr_subclass.input_content_resource) {
            $.each(metadata.described_entity.lr_subclass.input_content_resource, (i, icr) => {
                if (icr.media_type === 'http://w3id.org/meta-share/meta-share/audio'
                    || icr.media_type === 'http://w3id.org/meta-share/meta-share/image'
                    || icr.data_format.includes('http://w3id.org/meta-share/omtd-share/mp3')
                    || icr.data_format.includes('http://w3id.org/meta-share/omtd-share/wav')) {
                    $.each(icr.sample, (j, sam: ElgSample) => {
                        validSamples.push(sam);
                    });
                }
            });
        }

        if (validSamples.length) {
            // we have some valid samples
            sampleFetcher('config/sample-helpers.json').then((fetcher) => {
                // make a card for each sample
                for (const s of validSamples) {
                    const card = $('<section class="mdc-card sample-card"></section>');
                    const content = $('<div class="mdc-card__primary-action" data-mdc-auto-init="MDCRipple"></div>').appendTo(card);
                    if (s.tag) {
                        content.append($('<h2 class="mdc-typography--subtitle2 sample-tag"></h2>').text(s.tag));
                    }
                    var sampleTextElt = $('<p class="mdc-typography--body2 sample-text"></p>');
                    sampleTextElt.html("<em>Click to download</em>");

                    content.append(sampleTextElt);
                    s.textElement = sampleTextElt;
                    card.data('elg-sample', s);
                    target.append(card);
                }

                // set up event handlers
                $('#sample-spinner-holder').on('click', (e) => {
                    // modal overlays - ignore clicks
                    e.preventDefault();
                });

                const handleParams = (sample: ElgSample) => {
                    if (sample.sample_text) {
                        try {
                            const jsonSample = JSON.parse(sample.sample_text);
                            if (jsonSample.params) {
                                elgCommon.setParams(jsonSample.params);
                            }
                        } catch (e) {
                            // probably not JSON, ignore
                        }
                    }
                };
                target.on('click', '.sample-card', function (e) {
                    e.preventDefault();
                    const sample: ElgSample = $(this).data('elg-sample');
                    if (sample.sample_blob) {
                        ready(sample.sample_blob, sample.tag + " (" + sample.sample_blob.size + " bytes)");
                        handleParams(sample);
                    } else {
                        // remote sample at a URL
                        $('#mictab button, #filetab button').prop('disabled', true);
                        $('#sample-spinner-holder').removeClass('hidden');
                        ($('#sample-spinner')[0] as any).MDCCircularProgress.open();
                        fetcher.fetchSample(sampleType, sample.samples_location).done((data: Blob) => {
                            // cache content so we don't have to fetch it again
                            sample.sample_blob = data;
                            sample.textElement.text(data.size + " bytes");
                            ready(data, sample.tag + " (" + data.size + " bytes)");
                            handleParams(sample);
                        }).fail((errors: ElgStatus[]) => {
                            elgCommon.resolveErrors(errors).then((data) => {
                                $.each(data, (i, msg) => {
                                    msgsContainer.append($('<div class="alert alert-warning"></div>').text(msg));
                                });
                            });
                        }).always(function () {
                            $('#mictab button, #filetab button').prop('disabled', false);
                            $('#sample-spinner-holder').addClass('hidden');
                            ($('#sample-spinner')[0] as any).MDCCircularProgress.close();
                        });
                    }
                });
                toShow.removeClass("hidden");
            });
        }
    }

    return populateSamples;
}

export = metadataHandler;