import * as $ from "jquery";
import type {ElgText} from "data/elgText";
import type ElgCommon = require("./common");

export function responseHandler(elgCommon: ElgCommon, noAlternativesMessage: string): (data: any) => void {
    $('#test-again').on('click', (e) => {
        e.preventDefault();
        $('#elg-annotate-result').addClass('hidden');
        $('#elg-test-form').removeClass('hidden');
    });

    function handleResponse(data: any) {
        import("annotation-viewer").then((annotationViewer) => {
            if (data.response && data.response.hasOwnProperty("type") && data.response.type === 'texts') {
                $('#elg-test-form').addClass('hidden');
                $('#elg-annotate-result').removeClass('hidden');

                // re-enable the button
                $('#submit-form').prop('disabled', false);

                let texts: ElgText[] = data.response.texts;
                if(!texts || texts.length === 0) {
                    texts = [{content:noAlternativesMessage}];
                }
                annotationViewer.displaySegmentsOrAlternatives(texts, $('#alternatives'), $('#annotationSelector'));
            } else {
                const msgsContainer = $('#elg-messages');
                let error: ElgStatus;
                if(data.response && data.response.type) {
                    error = {
                        code:'elg.response.type.unsupported',
                        text:'Response type {0} not supported',
                        params:[data.response.type]
                    }
                } else {
                    error = {
                        code:'elg.response.invalid',
                        text:'Invalid response message'
                    }
                }
                elgCommon.resolveErrors([error]).then(resolvedErrors => {
                    $('<div class="alert alert-warning"></div>').text(resolvedErrors[0]).appendTo(msgsContainer);
                    // re-enable the button
                    $('#submit-form').prop('disabled', false);
                });
            }
        });
    }

    return handleResponse;
}