import * as $ from "jquery";

interface FetcherMap {
    [origin: string]: FetcherHelper;
}

interface FetcherHelper {
    url: URL;
    iframe: HTMLIFrameElement;
    signal: JQueryDeferred<boolean>;
}

class SampleFetcher {
    private sampleFetchers: FetcherMap = {};
    private expectedUrl = null;
    private currentDeferred: JQueryDeferred<string | Blob> = null;

    prepare(configUrl: string): JQueryPromise<SampleFetcher> {
        const deferred = $.Deferred<SampleFetcher>();
        $.ajax({
            url: configUrl,
            dataType: 'json'
        }).done((data: SampleHelperConfig) => {
            // set up listener for reply messages from our helpers
            const helperOrigins: { [origin: string]: boolean } = {};

            window.addEventListener("message", (e: MessageEvent<SampleMessage>) => {
                if (e.source === window.parent) {
                    // config message from parent - not for us
                    return;
                }
                if (this.currentDeferred) {
                    const u = new URL(this.expectedUrl);
                    const fetcher = this.sampleFetchers[u.origin];
                    if (fetcher && e.source === fetcher.iframe.contentWindow) {
                        // this is a good message, from the fetcher responsible
                        // for the URL we're expecting
                        const msg: SampleMessage = e.data;
                        if (msg.errors) {
                            this.currentDeferred.reject(msg.errors);
                        } else if (msg.url === this.expectedUrl) {
                            this.currentDeferred.resolve(msg.content);
                        } else {
                            console.log("Unexpected response message", msg);
                            this.currentDeferred.reject([{
                                'code': 'elg.sample.message.invalid',
                                'text': 'Invalid sample fetch message for URL {0}',
                                'params': [''+u],
                            } as ElgStatus]);
                        }
                    } else {
                        console.log(`Expecting URL ${this.expectedUrl} but received message from another fetcher`);
                        this.currentDeferred.reject([{
                            'code': 'elg.sample.message.invalid',
                            'text': 'Invalid sample fetch message for URL {0}',
                            'params': [''+u]
                        } as ElgStatus]);
                    }
                    this.currentDeferred = null;
                    this.expectedUrl = null;
                } else {
                    if (helperOrigins[e.origin] && $.isPlainObject(e.data) && e.data.type === 'ready') {
                        // this is a ready callback from a helper iframe
                        $.each(e.data.origins, (i, o) => {
                            this.sampleFetchers[o].signal.resolve(true);
                        });
                    } else {
                        console.log("Unexpected sample fetch message from " + e.source);
                    }
                }
            });

            var readySignals = []
            $.each(data.helpers, (i, entry) => {
                const helperUrl = new URL(entry.helper);
                const helperDeferred = $.Deferred<boolean>();
                readySignals.push(helperDeferred);
                const frame = $('<iframe></iframe>');
                helperOrigins[helperUrl.origin] = true;
                $.each(entry.origins, (j, origin) => {
                    this.sampleFetchers[origin] = {
                        url: helperUrl,
                        iframe: (frame[0] as HTMLIFrameElement),
                        signal: helperDeferred,
                    };
                });
                frame.attr('src', entry.helper).addClass('sample-helper-iframe').appendTo($('body'));
            });

            // wait for all fetcher iframes to be ready
            $.when.apply($, readySignals).then(() => {
                deferred.resolve(this);
            });
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log("Could not load sample helpers - all sample loads will be subject to CORS restrictions");
            deferred.resolve(this);
        });

        return deferred.promise();
    }

    /**
     * Fetch a sample, with the aid of a fetcher iframe if one is registered for this URL's origin, or directly via
     * $.ajax if not.
     * @param type text, audio or image
     * @param url the URL of the sample to fetch
     * @returns JQueryDeferred promise returning the fetched value (string if type == text, otherwise blob).  If an
     * error occurs during the fetch, the promise rejects with an array of errors suitable to pass to
     * ElgCommon.resolveErrors.
     */
    fetchSample(type: string, url: string) {
        const targetUrl = new URL(url);
        const deferred = $.Deferred<string | Blob>();
        const fetcher: FetcherHelper = this.sampleFetchers[targetUrl.origin];
        if (fetcher) {
            // delegate to the fetcher iframe
            this.expectedUrl = url;
            this.currentDeferred = deferred;
            fetcher.iframe.contentWindow.postMessage({
                type: type,
                url: url,
            } as SampleMessage, fetcher.url.origin);
        } else {
            // fetch ourselves
            const ajaxParams: JQueryAjaxSettings = {url: url};
            if (type === 'text') {
                ajaxParams.dataType = 'text';
            } else {
                ajaxParams.xhr = function () {
                    var xhr = new XMLHttpRequest();
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState === 2) {
                            if (xhr.status === 200) {
                                xhr.responseType = "blob";
                            } else {
                                xhr.responseType = "text";
                            }
                        }
                    };
                    return xhr;
                };
            }
            $.ajax(ajaxParams).done((data: string | Blob) => {
                deferred.resolve(data);
            }).fail((jqXHR, textStatus, errorThrown) => {
                console.log("Could not load sample", errorThrown);
                deferred.reject([{
                    'code': 'elg.sample.download.failed',
                    'text': 'Could not download sample data: {0}',
                    'params': [errorThrown]
                } as ElgStatus]);
            });
        }

        return deferred.promise();
    }
}

/**
 * Attempts to load sample helpers configuration from the given URL and sets up the infrastructure to use
 * the helpers to fetch samples.
 * @param configUrl URL from which to load the configuration - this must be on the same origin as the calling page,
 * or on an origin that serves it with the appropriate CORS headers.
 * @returns JQueryDeferred promise that will resolve to a SampleFetcher object with a fetchSample(type, url) method.
 * The promise will only resolve once all the sample fetcher iframes have loaded and are ready to process requests.
 */
function setup(configUrl): JQueryPromise<SampleFetcher> {
    return new SampleFetcher().prepare(configUrl);
}

export = setup;