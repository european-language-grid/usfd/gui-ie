import * as $ from "jquery";
import type {ElgText} from "data/elgText";
import type ElgCommon = require("./common");

export function responseHandler(elgCommon: ElgCommon, noAlternativesMessage: string): (data: any) => void {
    $('#test-again').on('click', (e) => {
        e.preventDefault();
        $('#image-canvas').prop('hidden', true);
        $('#filedata').text("No image loaded - upload file below");
        $('#elg-annotate-result').addClass('hidden');
        $('#elg-test-form').removeClass('hidden');
    });


    // assuming the bounding will be at *one* level in the texts hierarchy,
    // but not specifying which specific one
    function handleBoundingBoxes(text, count){
        if(text.texts){
            text.texts.forEach(inner_text=>{
                const features = inner_text.features
                if (features && features.bounding_box){
                    const coords = features.bounding_box || features.bbox || features.boundingBox
                    if (coords instanceof Array &&
                        coords.length === 4 &&
                        coords.every((val) => val instanceof Array && val.length === 2)){
                            count = count+1
                            handleBoundingBox(coords, count)
                    }
                }
                handleBoundingBoxes(inner_text, count)
            })
        }
    }

    function handleBoundingBox(coords, count){
        let canvas = $('#image-canvas')
        // @ts-ignore
        let context = canvas[0].getContext('2d');
        context.strokeStyle = "lime"
        context.lineWidth = 3
        context.fillStyle = "black"
        context.font = "bold 40px serif"

        let x = coords[0][0]
        let y = coords[0][1]

        let w = coords[2][0] - x
        let h = coords[2][1] - y

        context.strokeRect(x, y, w, h)
        context.strokeText(count.toString(), x+5, y+25)
        context.fillText(count.toString(), x+5, y+25)

    }

    function handleResponse(data: any) {
        import("annotation-viewer").then((annotationViewer) => {
            if (data.response && data.response.hasOwnProperty("type") && data.response.type === 'texts') {
                $('#elg-test-form').addClass('hidden');
                $('#elg-annotate-result').removeClass('hidden');

                // re-enable the button
                $('#submit-form').prop('disabled', false);

                let texts: ElgText[] = data.response.texts;
                if(!texts || texts.length === 0) {
                    texts = [{content:noAlternativesMessage}];
                }
                handleBoundingBoxes(data.response, 0)
                annotationViewer.displaySegmentsOrAlternatives(texts, $('#alternatives'), $('#annotationSelector'));
            } else {
                const msgsContainer = $('#elg-messages');
                let error: ElgStatus;
                if(data.response && data.response.type) {
                    error = {
                        code:'elg.response.type.unsupported',
                        text:'Response type {0} not supported',
                        params:[data.response.type]
                    }
                } else {
                    error = {
                        code:'elg.response.invalid',
                        text:'Invalid response message'
                    }
                }
                elgCommon.resolveErrors([error]).then(resolvedErrors => {
                    $('<div class="alert alert-warning"></div>').text(resolvedErrors[0]).appendTo(msgsContainer);
                    // re-enable the button
                    $('#submit-form').prop('disabled', false);
                });
            }
        });
    }

    return handleResponse;
}