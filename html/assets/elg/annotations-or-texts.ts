import * as $ from "jquery";
import type {ElgText} from "data/elgText";
import type ElgCommon = require("./common");

let text = '';
let texts: ElgText[] | null = null;

export function useText(t: string) {
    text = t;
}

export function useTexts(ts: ElgText[]) {
    texts = ts;
}

export function makeTexts(data: any): ElgText[] {
    let elgTexts: ElgText[] = [{
        content: "Wrong response type received",
    }];
    try {
        if (data.response.type === "annotations") {
            const txt: ElgText = {
                annotations: data.response.annotations,
                features: data.response.features,
            };
            if(texts) {
                txt.texts = texts;
            } else {
                txt.content = text;
            }
            elgTexts = [txt];
        } else if (data.response.type === "texts" && data.response.texts && data.response.texts.length) {
            elgTexts = data.response.texts;
        }
    } catch (e) {
        // do nothing, leave as defaults
        console.log(e);
    }
    return elgTexts;
}

export function responseHandler(elgCommon: ElgCommon): (data: any) => void {
    function handleResponse(data: any): void {
        import("annotation-viewer").then((annotationViewer) => {
            if (data.response && data.response.hasOwnProperty("type")
                && (data.response.type === 'annotations' || data.response.type === 'texts')) {

                $('#elg-test-form').addClass('hidden');
                $('#elg-annotate-result').removeClass('hidden');

                // re-enable the button
                $('#submit-form').prop('disabled', false);
                //$('#docView').replaceWith("<div id='docView'></div>");

                let elgTexts = makeTexts(data);

                annotationViewer.displaySegmentsOrAlternatives(elgTexts, $('#docView'), $('#annotationSelector'));

            } else {
                const msgsContainer = $('#elg-messages');
                let error: ElgStatus;
                if(data.response && data.response.type) {
                    error = {
                        code:'elg.response.type.unsupported',
                        text:'Response type {0} not supported',
                        params:[data.response.type]
                    }
                } else {
                    error = {
                        code:'elg.response.invalid',
                        text:'Invalid response message'
                    }
                }
                elgCommon.resolveErrors([error]).then((resolvedErrors) => {
                    $('<div class="alert alert-warning"></div>').text(resolvedErrors[0]).appendTo(msgsContainer);
                    // re-enable the button
                    $('#submit-form').prop('disabled', false);
                });
            }
        });
    }

    return handleResponse;
}