/*
 * Interface definitions for various types of objects we get back from ELG APIs.
 */

/**
 * Multilingual string - a map from language code to value.
 */
interface MLString {
    [lang: string]: string;
}

/**
 * The config JSON message sent by the top-level frame to the GUI.
 */
interface GuiConfig {
    Authorization?: string;
    ServiceUrl?: string;
    Language?: string;
    StyleCss?: string;
    ApiRecordUrl?: string;
}

/**
 * An ELG status message.
 */
interface ElgStatus {
    code: string;
    text: string;
    params?: string[];
    detail?: any;
}

/**
 * Service parameter definition in the metadata record.
 */
interface Parameter {
    parameter_name: string;
    parameter_label: MLString;
    parameter_description: MLString;
    parameter_type: string;
    multi_value: boolean;
    optional: boolean;
    default_value: string;
    enumeration_value: EnumValue[];
}

/**
 * Enum value definition for service parameter.
 */
interface EnumValue {
    value_name: string;
    value_label: MLString;
    value_description: MLString;
}

/**
 * Single "sample" definition in the metadata, with additional fields added by the sample handlers.
 */
interface ElgSample {
    tag: string;
    sample_text?: string;
    samples_location?: string;

    sample_blob?: Blob;
    textElement: JQuery;
}

/**
 * Message body sent by a sample helper back to our sample fetcher.
 */
interface SampleMessage {
    /**
     * The message type, either text, audio, possibly other values for sample
     * types, or "ready" if it's a ready response from a helper that's finished initializing.
     */
    type: string;

    /**
     * Only present if this is an error message.
     */
    errors?: ElgStatus[];

    /**
     * Only present if this is a "ready" message.
     */
    origins?: string[];

    /**
     * URL the helper was asked to fetch.
     */
    url?: string;

    /**
     * Fetched content.
     */
    content?: string | Blob;
}

/**
 * Definition of a single helper in sample-helpers.json
 */
interface SampleHelperDefinition {
    origins: string[];
    helper: string;
}

/**
 * Top level of sample-helpers.json file
 */
interface SampleHelperConfig {
    helpers: SampleHelperDefinition[];
}

/**
 * A single entry in the "classes" array of a classification response.
 */
interface ElgClassificationClass {
    class: string;
    score?: number;
}