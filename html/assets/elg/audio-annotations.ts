import * as $ from "jquery";

import WaveSurfer = require('wavesurfer/wavesurfer');
import WSRegions = require('wavesurfer/plugin/wavesurfer.regions');
import WSMinimap = require('wavesurfer/plugin/wavesurfer.minimap');
import WSTimeline = require('wavesurfer/plugin/wavesurfer.timeline');

import ElgCommon = require("./common");
import audioInput = require("./audio-input");
import type {ElgAnnotation, ElgAnnotations} from "data/elgText";

import {featureTable} from "annotation-viewer";
import {AnnotationSelector} from "data/annotationSelector";
import {Selector} from "views/selector";

/**
 * Global variable which can be populated by the host page if only
 * specific annotation types should be visible by default.  If empty
 * or unset, all annotation types are visible by default unless
 * hidden by elgDefaultHiddenTypes.
 */
declare var elgDefaultShownTypes: string[];

/**
 * Global variable which can be populated by the host page if
 * certain annotation types should not be visible by default.  If
 * empty or unset then all types are selected by default.  The logic
 * is that a given type T is shown by default if
 *
 * (elgDefaultShownTypes-is-undef-or-empty || elgDefaultShownTypes.includes(T)) &&
 * (elgDefaultHiddenTypes-is-undef-or-empty || !elgDefaultHiddenTypes.includes(T))
 */
declare var elgDefaultHiddenTypes: string[];

/**
 * Determines whether or not a given annotation type should be selected by default.
 * @param type the annotation type
 */
function shouldShow(type: string): boolean {
    return (
            typeof elgDefaultShownTypes === 'undefined' ||
            elgDefaultShownTypes.length === 0 ||
            elgDefaultShownTypes.indexOf(type) >= 0)
        && (typeof elgDefaultHiddenTypes === 'undefined' ||
            elgDefaultHiddenTypes.indexOf(type) < 0)
}

export function responseHandler(elgCommon: ElgCommon): (data: any) => void {
    const surfer = WaveSurfer.create({
        container: '#waveform',
        height: 100,
        pixelRatio: 1,
        scrollParent: true,
        normalize: true,
        backend: 'MediaElement',
        plugins: [
            WSRegions.create({
                dragSelection: false
            }),
            WSMinimap.create({
                height: 30,
                waveColor: '#ddd',
                progressColor: '#999',
                cursorColor: '#999'
            }),
            WSTimeline.create({
                container: '#wave-timeline'
            })
        ]
    });
    $('#test-again').on('click', (e) => {
        e.preventDefault();
        $('#elg-annotate-result').addClass('hidden');
        clearSelection();
        if(surfer) {
            surfer.clearRegions();
        }
        $('#elg-test-form').removeClass('hidden');
    });

    $('#play-audio').on('click', (e) => {
        e.preventDefault();
        surfer.play();
    });
    $('#pause-audio').on('click', (e) => {
        e.preventDefault();
        surfer.pause();
    })
    surfer.on('play', () => {
        $('#play-audio').addClass('hidden');
        $('#pause-audio').removeClass('hidden');
    });
    surfer.on('pause', () => {
        $('#pause-audio').addClass('hidden');
        $('#play-audio').removeClass('hidden');
    });

    $('#zoom-in').on('click', (e) => {
        e.preventDefault();
        surfer.zoom(surfer.params.minPxPerSec * 2);
    });
    $('#zoom-out').on('click', (e) => {
        e.preventDefault();
        surfer.zoom(surfer.params.minPxPerSec / 2);
    });

    function zoomHandler(pxPerSec) {
        let waveWidth = $('#waveform').width();
        let calcWidth = pxPerSec * surfer.params.pixelRatio * surfer.getDuration();
        $('#zoom-out').prop('disabled', waveWidth >= calcWidth);
    }

    surfer.on('zoom', zoomHandler);

    let selectedRegions = [];

    function clearSelection() {
        for(let r of selectedRegions) {
            deselect(r);
        }
    }

    function select(region) {
        if(!$(region.element).hasClass('hidden') && selectedRegions.indexOf(region) < 0) {
            selectedRegions.push(region);
            let ann = region.data.annotation;
            $(region.element).addClass('selected');
            let detailElt = region.data.detailElt;
            if(!detailElt) {
                detailElt = $('<div></div>');
                let headerElt = $('<div></div>')
                    .append($('<strong></strong>').text(region.attributes.annType))
                    .append(document.createTextNode(' (' + ann.start + '-' + ann.end + ')'));
                detailElt.append(headerElt);
                if (ann.features && !$.isEmptyObject(ann.features)) {
                    featureTable(ann.features, detailElt);
                } else {
                    detailElt.append($('<div><i>No Features</i></div>'));
                }
                region.data.detailElt = detailElt;
            }
            detailElt.appendTo($('#annotation-detail'));
        }
    }

    function deselect(region) {
        let regionIdx = selectedRegions.indexOf(region);
        if(regionIdx >= 0) {
            selectedRegions.splice(regionIdx, 1);
            $(region.element).removeClass('selected');
            if(region.data.detailElt) {
                region.data.detailElt.detach();
            }
        }
    }

    surfer.on('region-click', (region) => {
        clearSelection();
        select(region);
    });

    surfer.on('region-in', select);
    surfer.on('region-out', deselect);

    $('#selectFollowsPlayhead').on('click', function(e) {
        if(this.MDCSwitch.selected) {
            surfer.on('region-in', select);
            surfer.on('region-out', deselect);
        } else {
            surfer.un('region-in', select);
            surfer.un('region-out', deselect);
        }
    });

    function handleResponse(data) {
        if (data.response && data.response.hasOwnProperty("type")
            && data.response.type === 'annotations') {

            $('#elg-test-form').addClass('hidden');
            $('#elg-annotate-result').removeClass('hidden');

            // re-enable the button
            $('#submit-form').prop('disabled', false);

            // load audio
            surfer.load(audioInput.blobUrl());
            surfer.once('ready', function () {
                zoomHandler(surfer.params.minPxPerSec);
                let annotationSelector = new AnnotationSelector();
                let annotations: ElgAnnotations = {};
                let allAnnotations: ElgAnnotation[] = [];
                for(let annType in data.response.annotations) {
                    if(data.response.annotations.hasOwnProperty(annType)) {
                        let annsList: ElgAnnotation[] = data.response.annotations[annType];
                        if(annsList && annsList.length) {
                            annotations[annType] = annsList;
                            annotationSelector.addType(annType, shouldShow(annType));
                            let colour = annotationSelector.typeColour(annType);
                            for (let a of annsList) {
                                if (a.end < a.start) {
                                    // ensure start and end are the right way round
                                    [a.start, a.end] = [a.end, a.start];
                                }
                                a["wsRegion"] = surfer.addRegion({
                                    start: a.start,
                                    end: a.end,
                                    drag: false,
                                    resize: false,
                                    color: colour,
                                    attributes: {
                                        annType: annType
                                    },
                                    data: {
                                        annotation: a
                                    }
                                });
                                allAnnotations.push(a);
                            }
                        }
                    }
                }

                if(allAnnotations.length) {
                    // at this point we have created (possibly overlapping) regions for all annotations,
                    // now we need to resolve overlaps.  First sort the list of all annotations by length,
                    // longest first
                    allAnnotations.sort(function (a, b) {
                        return (b.end - b.start) - (a.end - a.start);
                    });

                    // now build "layers" of non-overlapping annotations - start with a single empty layer,
                    // then for each annotation see whether it will fit in that layer without overlapping.
                    // If so, we're done, if not, check the next layer.  If we run out of layers, create a
                    // new one.  Continue this process until all annotations are dealt with.
                    let layers: ElgAnnotation[][] = [];
                    loopAnn: for (let ann of allAnnotations) {
                        for (let layer of layers) {
                            // binary search to find the last annotation whose end is <= our start
                            let l = 0;
                            let r = layer.length - 1;
                            while (l <= r) {
                                let mid = Math.floor((l + r) / 2);
                                if (layer[mid].end > ann.start) {
                                    r = mid - 1;
                                } else {
                                    l = mid + 1;
                                }
                            }
                            // l is now the point at which we would insert this annotation into the
                            // current layer *provided* it does not overlap
                            if (l === layer.length || layer[l].start >= ann.end) {
                                layer.splice(l, 0, ann);
                                continue loopAnn;
                            }
                        }
                        // we haven't found anywhere to fit the annotation, create a new layer
                        layers.push([ann]);
                    }

                    // now adjust the regions' vertical height and position to match the
                    // layer stacking
                    let layerHeight = ((1 / layers.length) * 100) + '%';
                    for (let n = 0; n < layers.length; n++) {
                        let layerTop = ((1 / layers.length) * (layers.length - n - 1) * 100) + '%';
                        for (let ann of layers[n]) {
                            const region = ann["wsRegion"];
                            region.style(region.element, {
                                height: layerHeight,
                                top: layerTop
                            });
                        }
                    }

                    // create the annotation types selector
                    Selector.create(annotationSelector, $('#annotationSelector'));
                    $('#annotation-detail').empty().append($('<h3>Details</h3>'));

                    // enable showing and hiding of annotation types
                    $(annotationSelector).on("visible:changed", function () {
                        for (let annType in annotations) {
                            if (annotations.hasOwnProperty(annType)) {
                                $('#waveform region[data-region-annType="'
                                    + annType.replace('"', '\\"') + '"]')
                                    .toggleClass('hidden', !annotationSelector.isAnnotationVisible(annType));
                            }
                        }
                    });
                }
            });
        } else {
            const msgsContainer = $('#elg-messages');
            let error: ElgStatus;
            if (data.response && data.response.type) {
                error = {
                    code: 'elg.response.type.unsupported',
                    text: 'Response type {0} not supported',
                    params: [data.response.type]
                }
            } else {
                error = {
                    code: 'elg.response.invalid',
                    text: 'Invalid response message'
                }
            }
            elgCommon.resolveErrors([error]).then(function (resolvedErrors) {
                $('<div class="alert alert-warning"></div>').text(resolvedErrors[0]).appendTo(msgsContainer);
                // re-enable the button
                $('#submit-form').prop('disabled', false);
            });
        }
    }

    return handleResponse;
}