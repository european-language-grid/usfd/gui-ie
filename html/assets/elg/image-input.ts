import * as $ from "jquery";

import type ElgCommon = require("./common");

let currentBlob: Blob = null;
let objectUrl: string = null;
let curBlobContentType: string = null;
let blobDescription: string = '';


export function imageReady(blob: Blob, desc: string): void {
    if (objectUrl != null) {
        URL.revokeObjectURL(objectUrl);
        objectUrl = null;
    }
    currentBlob = blob;
    curBlobContentType = blob.type;
    blobDescription = desc;
    objectUrl = URL.createObjectURL(currentBlob);
    canvasReady(objectUrl)
    $('#filedata').text(blobDescription);
    $('#submit-form').prop('disabled', false);
}

function canvasReady(object_url) {
    let img = new Image()
    img.onload = () => {
        let canvas = $('#image-canvas')
        canvas.prop("width", img.width)
        canvas.prop("height", img.height)
        let context = (canvas[0] as HTMLCanvasElement).getContext('2d');
        context.drawImage(img, 0, 0)
        canvas.prop("hidden", false)
    }
    img.src = object_url
}


export function setup(elgCommon: ElgCommon, handleResponse: (r: any) => void): void {
    // set up the file upload button
    $('#fileinput').on('change', (e) => {
        const files = (e.target as HTMLInputElement).files;
        if (files) {
            imageReady(files[0], "File " + files[0].name + " (" + files[0].size + " bytes)");
        }
    });
    $("#selectfile").on('click', (e) => {
        e.preventDefault();
        $('#fileinput').trigger('click');
    });

    // set up the submit form button
    $("#submit-form").on('click', function (e) {
        e.preventDefault();
        // disable the button until the REST call returns
        $('#submit-form').prop('disabled', true);
        $('#elg-messages').empty();

        elgCommon.callService(currentBlob, curBlobContentType, handleResponse);
        return false;
    });
}