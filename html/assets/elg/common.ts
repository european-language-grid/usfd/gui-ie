import * as $ from "jquery";
import * as _ from "underscore";
// @ts-ignore
import * as mdc from "material-components-web";

class ElgCommon {
    
    private injectedCss: boolean;
    private fetchedParams: boolean;
    private serviceInfo: GuiConfig;
    private readonly metadataCallback: (md: any, common: ElgCommon) => void;
    private readonly afterErrorCallback: () => void;
    private readonly submitProgress: mdc.MDCLinearProgress;
    private collator: (a: string, b: string) => number;
    private i18nUrl: URL;
    private paramsForm: JQuery;
    
    constructor(readyCallback: () => void,
                afterErrorCallback: () => void,
                submitProgress,
                metadataCallback: (md: any, common: ElgCommon) => void) {
        this.injectedCss = false;
        this.fetchedParams = false;
        this.serviceInfo = {ServiceUrl: null, Authorization: null};
        this.metadataCallback = metadataCallback;
        this.afterErrorCallback = afterErrorCallback;
        this.submitProgress = submitProgress;
        this.collator = function(a, b) { return a.localeCompare(b); }

        // Listen to messages from parent window
        window.addEventListener('message', (e) => {
            if ((e.source === window.parent)
                && (window.location.origin === e.origin)
                && e.data) {

                this.serviceInfo = JSON.parse(e.data);
                if (this.serviceInfo.ServiceUrl.indexOf('async/') >= 0) {
                    this.i18nUrl = new URL('../../../i18n/resolve', this.serviceInfo.ServiceUrl);
                } else {
                    this.i18nUrl = new URL('../../i18n/resolve', this.serviceInfo.ServiceUrl);
                }
                if (this.serviceInfo.Language) {
                    this.i18nUrl.searchParams.append('lang', this.serviceInfo.Language);
                } else {
                    // store language away for future use
                    this.serviceInfo.Language = navigator.language.split(/-/, 2)[0];
                }
                this.collator = new Intl.Collator(this.serviceInfo.Language).compare;
                if (!this.injectedCss) {
                    // inject CSS
                    var elgCss = $('<link type="text/css" rel="stylesheet" media="screen,print">');
                    elgCss.attr('href', this.serviceInfo.StyleCss);
                    $('head').append(elgCss);
                    this.injectedCss = true;
                }
                if (!this.fetchedParams) {
                    this.fetchParams(readyCallback);
                }
            }
        });
        // and tell the parent we're ready for a message
        setTimeout( () => {
            window.parent.postMessage("GUI:Ready for config", window.location.origin);
        }, 500);
    }

    withAuthSettings(obj: JQueryAjaxSettings) : JQueryAjaxSettings {
        if (this.serviceInfo.Authorization) {
            obj.xhrFields = {withCredentials: true};
            obj.headers = {Authorization: this.serviceInfo.Authorization};
        }
        return obj;
    }

    private i(msg: MLString) {
        // return a suitable entry from an object keyed by language code
        // - first preference - user's configured language
        // - second preference - English
        // - fallback - first language in iteration order (e.g. if the object
        //   only has one language in it and that language isn't English)
        if(msg.hasOwnProperty(this.serviceInfo.Language)) {
            return msg[this.serviceInfo.Language];
        } else if(msg.hasOwnProperty('en')) {
            return msg.en;
        } else {
            for (const p in msg) {
                if(msg.hasOwnProperty(p)) {
                    return msg[p];
                }
            }
            return "No value found";
        }
    };

    /**
     * Create a single <input> or <select> element suitable for the given parameter definition.
     * @param p the parameter as taken from the service metadata
     * @returns the form field
     */
    private createValueInput(p: Parameter): HTMLElement {
        let input: JQuery;
        if (p.parameter_type === 'http://w3id.org/meta-share/meta-share/boolean') {
            // true/false select
            input = $('<select>');
            $('<option>false</option>').prop('selected', p.default_value === 'false').appendTo(input);
            $('<option>true</option>').prop('selected', p.default_value !== 'false').appendTo(input);
        } else if (p.enumeration_value && p.enumeration_value.length) {
            // enumerated parameter
            input = $('<select>');
            p.enumeration_value.sort((a, b) => this.collator(a.value_name, b.value_name));
            for(let j = 0; j < p.enumeration_value.length; j++) {
                const val = p.enumeration_value[j];
                const opt = $('<option>');
                opt.attr('value', val.value_name);
                opt.text(this.i(val.value_label));
                opt.attr('title', this.i(val.value_description));
                if(p.default_value && p.default_value == val.value_name) {
                    opt.prop('selected', true);
                }
                opt.appendTo(input);
            }
        } else {
            input = $('<input>').prop('required', true);
            if (p.default_value) {
                input.attr('value', p.default_value);
            }
            if (p.parameter_type === 'http://w3id.org/meta-share/meta-share/integer') {
                input.attr('type', 'number').attr('step', 1);
            } else if (p.parameter_type === 'http://w3id.org/meta-share/meta-share/float') {
                input.attr('type', 'number').attr('step', 'any');
            } else if (p.parameter_type === 'http://w3id.org/meta-share/meta-share/url1') {
                input.attr('type', 'url');
            } else {
                // treat anything else as string
                input.attr('type', 'text');
            }
        }
        if (p.multi_value) {
            input.attr('name', p.parameter_name + '[]')
        } else {
            input.attr('name', p.parameter_name);
        }
        input.addClass('param-value-input');
        return input[0];
    }

    /**
     * Fix up the rows and buttons for a multi-valued parameter to match the given list of input fields.
     * @param section the tbody section for this parameter
     * @param inputs the correct list of value input elements, must not be empty
     */
    fixUpMultiValue(section: JQuery, inputs: HTMLElement[]) {
        let rows = section.find('tr');
        const addButton = section.find('button.param-add');
        // delete trailing rows, if there are more rows than inputs
        rows.slice(inputs.length).remove();
        // add new rows if required
        for(let i = rows.length; i < inputs.length; i++) {
            section.append($('<tr><td></td><td></td><td class="param-value"></td>' +
                '<td><button class="mdc-icon-button material-icons param-delete" title="Delete this value">remove_circle</button></td>' +
                '<td></td></tr>'));
        }
        // re-select rows including the new ones
        rows = section.find('tr');

        // put the input elements in the right rows
        const valueCells = section.find('td.param-value');
        for(let j = 0; j < inputs.length; j++) {
            $(valueCells[j]).empty().append(inputs[j]);
        }

        // move the add button to the last row only
        addButton.appendTo(rows.last().find('td').last());
        // disable delete if there's only one row left
        rows.find('button.param-delete').prop('disabled', rows.length === 1);
    }

    buildParamsForm(params: Parameter[]): JQuery {
        const this_ = this;
        const f = $('<form></form>');
        const tbl = $('<table></table>').appendTo(f);

        // event listeners
        // toggle disabled based on the "use this param" checkbox
        f.on('change', 'input.param-use', function(e) {
            const cb = $(this);
            const disable = !cb.prop('checked');
            const section = cb.closest('tbody');
            section.find('input, button, select').not('.param-use').prop('disabled', disable);
            // but make sure multi valued delete buttons are still disabled regardless of checkbox state
            // if there's only one value in the section
            const delButtons = section.find('button.param-delete');
            if(delButtons.length === 1) {
                delButtons.prop('disabled', true);
            }
        });

        // handle the add/remove buttons for a multi-value parameter
        f.on('click', 'button.param-add', function(e) {
            e.preventDefault();
            const btn = $(this);
            const section = btn.closest('tbody');
            const inputs = section.find('.param-value-input').toArray();
            inputs.push(this_.createValueInput(section.data('param')));
            this_.fixUpMultiValue(section, inputs);
        });

        f.on('click', 'button.param-delete', function(e) {
            e.preventDefault();
            const btn = $(this);
            // delete this row's input element
            const row = btn.closest('tr');
            row.find('.param-value-input').remove();
            // gather the remaining inputs
            const section = row.closest('tbody');
            const inputs = section.find('.param-value-input').toArray();
            // and fix up
            this_.fixUpMultiValue(section, inputs);
        });

        // add extra cells if ANY param is multi valued
        let hasMultiValuedParam: boolean = false;
        for (const param of params) {
            if(param.multi_value) {
                hasMultiValuedParam = true;
                break;
            }
        }

        const head = $('<thead><tr><th title="Should we send this parameter?" style="text-align: center">Use?</th><th>Name</th><th>Value</th></tr></thead>').appendTo(tbl);
        if(hasMultiValuedParam) {
            head.find('tr').append($('<th>&nbsp;</th><th>&nbsp;</th>'));
        }
        $.each(params, (idx, p) => {
            // make a separate tbody per param so we can have multiple rows for multi_value params
            const section = $('<tbody></tbody>').appendTo(tbl);
            section.data('param', p);
            const row = $('<tr></tr>').appendTo(section);
            const useCell = $('<td></td>').appendTo(row);
            useCell.append($('#param-checkbox-template').children().clone());
            new mdc.checkbox.MDCCheckbox(useCell.children()[0]);
            const useCheckbox = useCell.find('input');
            if(!p.optional) {
                // required parameter - box is always checked
                useCheckbox.prop('checked', true)
                    .prop('disabled', true);
                useCell.children().attr('title', 'This parameter is required');
            }

            const nameCell = $('<td></td>').appendTo(row);
            nameCell.text(p.parameter_label ? this.i(p.parameter_label) : p.parameter_name);
            const valueCell = $('<td></td>').addClass('param-value').appendTo(row);
            if(p.parameter_description) {
                const localizedDesc = this.i(p.parameter_description);
                nameCell.attr('title', localizedDesc);
                valueCell.attr('title', localizedDesc);
            }

            const valueInput = this.createValueInput(p);
            valueCell.append(valueInput);
            if(p.multi_value) {
                // add button cells
                $('<td><button class="mdc-icon-button material-icons param-delete" title="Delete this value">remove_circle</button></td>').appendTo(row);
                $('<td><button class="mdc-icon-button material-icons param-add" title="Add another value">add_circle</button></td>').appendTo(row);
                // then fix up the buttons
                this.fixUpMultiValue(section, [valueInput]);
            } else if(hasMultiValuedParam) {
                row.append($('<td>&nbsp;</td><td>&nbsp;</td>'))
            }

            useCheckbox.trigger('change');
        });

        return f;
    }

    fetchParams(readyCallback: () => void): void {
        if (this.serviceInfo.ApiRecordUrl) {
            $.get(this.withAuthSettings({
                url: this.serviceInfo.ApiRecordUrl,
                success: (metadata, textStatus) => {
                    const paramsDiv = $('#service-params');
                    if (paramsDiv.length &&
                        metadata.described_entity &&
                        metadata.described_entity.lr_subclass &&
                        metadata.described_entity.lr_subclass.parameter &&
                        metadata.described_entity.lr_subclass.parameter.length) {
                        // this service takes parameters - create the form
                        const paramsForm = this.buildParamsForm(metadata.described_entity.lr_subclass.parameter);

                        $('<div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-10">')
                            .appendTo(paramsDiv.addClass('mdc-layout-grid__inner'))
                            .append('<h3 class="mdc-typography--subtitle2">Additional parameters</h3>')
                            .append(paramsForm);
                        paramsDiv.css('display', 'block');
                        this.paramsForm = paramsForm;
                    }
                    // hook for clients to configure themselves from the metadata
                    if(this.metadataCallback) {
                        this.metadataCallback(metadata, this);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#service-params')
                        .append($('<div class="alert alert-warning"></div>')
                            .text("Failed to fetch metadata - calls may fail if the service requires parameters"))
                        .css('display', 'block');
                },
                complete: function () {
                    readyCallback();
                }
            }));
        } else {
            // can't fetch parameter info, so we're ready now
            readyCallback();
        }
        this.fetchedParams = true;
    }

    setParams(values: object): void {
        const useCheckboxUpdates: [JQuery, boolean][] = [];
        $('#service-params tbody').each((n, e) => {
            const section = $(e);
            const p: Parameter = section.data('param');
            if(values.hasOwnProperty(p.parameter_name)) {
                if(p.optional) {
                    // send this parameter
                    useCheckboxUpdates.push([section.find('.param-use'), true]);
                }
                if(p.multi_value) {
                    const thisParamValues: any[] = ($.isArray(values[p.parameter_name]) ? values[p.parameter_name] : [values[p.parameter_name]]);
                    const inputs = section.find('.param-value-input').toArray();
                    for(let i = inputs.length; i < thisParamValues.length; i++) {
                        inputs.push(this.createValueInput(p));
                    }
                    inputs.length = thisParamValues.length;
                    for(let i = 0; i < thisParamValues.length; i++) {
                        $(inputs[i]).val(''+thisParamValues[i]);
                    }
                    this.fixUpMultiValue(section, inputs);
                } else {
                    // not multi-value, just set the single value field
                    section.find('.param-value-input').val(''+values[p.parameter_name]);
                }
            } else {
                // this parameter is not specified
                if(p.optional) {
                    // - if optional, don't send it
                    useCheckboxUpdates.push([section.find('.param-use'), false]);
                } else if(p.default_value) {
                    // - if required, and has a default, send the default
                    const input = section.find('.param-value-input')[0];
                    $(input).val(p.default_value);
                    if(p.multi_value) {
                        // remove additional value rows for this parameter
                        this.fixUpMultiValue(section, [input]);
                    }
                } else {
                    // - if required and doesn't have a default, leave alone
                }
            }
        });

        if(useCheckboxUpdates.length) {
            // update the "use" checkboxes in the next event loop cycle, to ensure the MDC event handlers fire properly
            setTimeout(() => {
                for(let [cb, val] of useCheckboxUpdates) {
                    cb.prop('checked', val);
                    cb.trigger("change");
                }
            }, 0);
        }
    }

    private localResolveErrors(errors: ElgStatus[]): JQueryDeferred<string[]> {
        const placeholder = /{(\d+)}/g;
        const resolvedErrors: string[] = $.map(errors, function (e) {
            return e.text.replace(placeholder, function (match, num) {
                return e.params[parseInt(num, 10)]
            });
        });
        return $.Deferred<string[]>().resolve(resolvedErrors);
    }

    resolveErrors(errors: ElgStatus[]): JQueryPromise<string[]> {
        if (this.i18nUrl) {
            return $.post({
                url: this.i18nUrl.href,
                data: JSON.stringify(errors),
                dataType: 'json',
                contentType: 'application/json',
            }).fail((xhr, status, errorThrown) => {
                console.log(errorThrown);
                return this.localResolveErrors(errors);
            }) as JQueryPromise<string[]>;
        } else {
            return this.localResolveErrors(errors);
        }
    }

    ajaxErrorHandler() {
        return (jqXHR, textStatus, errorThrown) => {
            let errors = [];
            const responseJSON = jqXHR.responseJSON;
            const msgsContainer = $('#elg-messages');
            if (this.submitProgress) {
                this.submitProgress.close();
            }
            $('#process-state').text('\u00A0');
            if (responseJSON && responseJSON.hasOwnProperty("failure")) {
                errors = responseJSON.failure.errors;
                this.resolveErrors(errors).then((data) => {
                    $.each(data, (i, msg) => {
                        msgsContainer.append($('<div class="alert alert-warning"></div>').text(msg));
                    });
                    this.afterErrorCallback();
                });
            } else {
                // this should be i18n'd too really
                msgsContainer.append($('<div class="alert alert-warning">Unknown error occurred</div>'));
                this.afterErrorCallback();
            }
        }
    }

    callService(data: any, contentType: string, responseHandler: (r: any) => void) {
        const errorHandler = this.ajaxErrorHandler();

        const pollForResult = (pollUrl: string) => {
            let lastUpdate = null;

            const doPoll = () => {
                $.get(this.withAuthSettings({
                    url: pollUrl,
                    success: (respData: any, textStatus) => {
                        if (respData.progress) {
                            if(!_.isEqual(respData.progress, lastUpdate)) {
                                if (this.submitProgress) {
                                    this.submitProgress.determinate = (respData.progress.percent > 0);
                                    this.submitProgress.progress = respData.progress.percent / 100.0;
                                }
                                if(respData.progress.message) {
                                    this.resolveErrors([respData.progress.message]).then(function (msgs) {
                                        $('#process-state').text(msgs[0]);
                                    });
                                }
                                lastUpdate = respData.progress;
                            }
                            // schedule the next poll
                            setTimeout(doPoll, 2000);
                        } else {
                            if (this.submitProgress) {
                                this.submitProgress.close();
                            }
                            $('#process-state').text('\u00A0');
                            responseHandler(respData);
                        }

                        return false;
                    },
                    error: errorHandler
                }));
            }

            setTimeout(doPoll, 2000);
        };

        let targetUrl = this.serviceInfo.ServiceUrl;
        // add parameters if there are any
        if(this.paramsForm) {
            if((this.paramsForm[0] as HTMLFormElement).reportValidity()) {
                if(typeof data === 'object' && contentType.includes('json')) {
                    // insert params into the JSON message
                    data.params = data.params || {};
                    for(let {name: paramName, value: paramValue} of this.paramsForm.serializeArray()) {
                        if(paramName.endsWith('[]')) {
                            // multi valued
                            const realName = paramName.substring(0, paramName.length - 2);
                            let curValue = data.params[realName];
                            if(!$.isArray(curValue)) {
                                curValue = [];
                                data.params[realName] = curValue;
                            }
                            curValue.push(paramValue);
                        } else {
                            data.params[paramName] = paramValue;
                        }
                    }
                } else {
                    // send params in the query string
                    const queryString = this.paramsForm.serialize();
                    if (queryString) {
                        if (targetUrl.indexOf('?') >= 0) {
                            targetUrl += '&';
                        } else {
                            targetUrl += '?';
                        }
                        targetUrl += queryString;
                    }
                }
            } else {
                $('#elg-messages').append($('<div class="alert alert-warning">Service parameter settings are not valid</div>'));
                this.afterErrorCallback();
                return;
            }
        }
        if(typeof data === 'object' && contentType.includes('json')) {
            // convert to JSON
            data = JSON.stringify(data);
        }
        $.post(this.withAuthSettings({
            url: targetUrl,
            data: data,
            success: (respData, textStatus) => {
                if (respData.response && respData.response.type == 'stored') {
                    // async response, start polling
                    if (this.submitProgress) {
                        this.submitProgress.open();
                        this.submitProgress.determinate = false;
                        this.submitProgress.progress = 0;
                    }
                    $('#process-state').text('Processing');
                    pollForResult(respData.response.uri);
                } else {
                    if (this.submitProgress) {
                        this.submitProgress.close();
                    }
                    // sync response, handle it now
                    responseHandler(respData);
                }
                return false;
            },

            error: errorHandler,
            contentType: contentType,
            processData: false
        }));
    }

    /**
     * Replace the content of the given destination element with the given text string,
     * but convert newlines to HTML br tags to preserve line breaks.
     * @param destination the target element as a jquery object
     * @param text the text to use as its content
     */
    static setTextWithLineBreaks(destination: JQuery, text: string) {
        destination.empty();
        const lines = text.split("\n");
        for(let j = 0; j < lines.length; j++) {
            if(j > 0) {
                destination.append(document.createElement('br'));
            }
            destination.append(document.createTextNode(lines[j]));
        }
    }
}

export = ElgCommon;