import * as $ from "jquery";
import type ElgCommon = require("./common");

export function responseHandler(elgCommon: ElgCommon): (data: any) => void {
    function handleResponse(data: any): void {
        if (data.response && data.response.type == 'classification') {
            $('#elg-test-form').addClass('hidden');
            $('#elg-annotate-result').removeClass('hidden');

            // re-enable the button
            $('#submit-form').prop('disabled', false);

            // handle classes
            const responseClasses: ElgClassificationClass[] = data.response.classes;
            const classesTable = $('#classes-table');
            classesTable.empty();
            if (responseClasses) {
                const tblBody = $('<tbody></tbody>');
                let hasScore = false;
                for (let cls of responseClasses) {
                    var clsRow = $('<tr></tr>');
                    $('<td></td>').text(cls.class).appendTo(clsRow);
                    if (cls.score) {
                        hasScore = true;
                        $('<td></td>').text(cls.score).appendTo(clsRow);
                    }
                    tblBody.append(clsRow);
                }
                if (hasScore) {
                    // if we have two columns, add a header
                    classesTable.append($('<thead><tr><th>Class name</th><th>Score</th></tr></thead>'));
                }
                classesTable.append(tblBody);
            } else {
                classesTable.append('<tbody><tr><td><em>No classes found!</em></td></tr></tbody>');
            }
        } else {
            const msgsContainer = $('#elg-messages');
            let error: ElgStatus;
            if (data.response && data.response.type) {
                error = {
                    code: 'elg.response.type.unsupported',
                    text: 'Response type {0} not supported',
                    params: [data.response.type]
                }
            } else {
                error = {
                    code: 'elg.response.invalid',
                    text: 'Invalid response message'
                }
            }
            elgCommon.resolveErrors([error]).then(function (resolvedErrors) {
                $('<div class="alert alert-warning"></div>').text(resolvedErrors[0]).appendTo(msgsContainer);
                // re-enable the button
                $('#submit-form').prop('disabled', false);
            });
        }
    }

    return handleResponse;
}