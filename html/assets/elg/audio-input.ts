import * as $ from "jquery";

import WaveSurfer = require('wavesurfer/wavesurfer');
import type ElgCommon = require("./common");

declare var WebAudioRecorder;

declare var elgAudioFormat: string;
declare var elgAudioChannels: number;
declare var elgAudioSampleRate: number;

let currentBlob: Blob = null;
let objectUrl: string = null;
let curBlobContentType: string = null;
let blobDescription: string = '';
let surfer: WaveSurfer = null;

export function audioReady(blob: Blob, desc: string): void {
    if (objectUrl != null) {
        URL.revokeObjectURL(objectUrl);
        objectUrl = null;
    }
    currentBlob = blob;
    curBlobContentType = blob.type;
    // fix for broken chrome mime type for MP3
    if (curBlobContentType == 'audio/mp3') {
        curBlobContentType = 'audio/mpeg';
    }
    blobDescription = desc;
    $('#filedata').text(blobDescription);
    surfer.load(blobUrl());
    $('#submit-form').prop('disabled', false);
}

export function setup(elgCommon: ElgCommon, handleResponse: (r: any) => void): void {
    // set up the file upload button
    $('#fileinput').on('change', (e) => {
        const files = (e.target as HTMLInputElement).files;
        if(files) {
            audioReady(files[0], "File " + files[0].name + " (" + files[0].size + " bytes)");
        }
    });
    $("#selectfile").on('click', (e) => {
        e.preventDefault();
        $('#fileinput').trigger('click');
    });

    // set up the mic button
    const progressbar = (document.getElementById('progressbar') as any).MDCLinearProgress;
    let gumStream: MediaStream; 			//stream from getUserMedia()
    let recorder: any; 						//WebAudioRecorder object
    let input: MediaStreamAudioSourceNode; //MediaStreamAudioSourceNode  we'll be recording
    function stopRecording() {
        //stop microphone access
        gumStream.getAudioTracks()[0].stop();
        $('#stoprecord').addClass('hidden');
        $('#recordmic').removeClass('hidden');
        $('#recordstate').text('Encoding...'); //.removeClass('hidden');
        progressbar.progress = 0;
        progressbar.determinate = true;
        progressbar.open();
        recorder.finishRecording();
    }
    $("#stoprecord").on('click', function(e) {
        e.preventDefault();
        stopRecording();
    });
    $("#recordmic").on('click', function(e) {
        e.preventDefault();
        const constraints: MediaStreamConstraints = {audio: true, video: false};
        navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
            $('#recordmic').addClass('hidden');
            $('#recordmic').prop('disabled', true);
            $('#stoprecord').removeClass('hidden');
            $('#stoprecord').prop('disabled', false);
            const ctxOptions: AudioContextOptions = {};
            if(elgAudioSampleRate) {
                ctxOptions.sampleRate = elgAudioSampleRate;
            }
            const context = new AudioContext(ctxOptions);
            // store stream for use in stopRecording
            gumStream = stream;
            input = context.createMediaStreamSource(stream);
            recorder = new WebAudioRecorder(input, {
                workerDir: "web-audio-recorder/", // must end with slash
                encoding: (elgAudioFormat || "mp3"),
                numChannels: (elgAudioChannels || 2), //2 is the default, mp3 encoding supports only 2
            });
            recorder.onEncodingProgress = function(rec, progress) {
                progressbar.progress = progress;
            };
            recorder.onEncodingCanceled = function(rec) {
                rec.close();
            };
            recorder.onComplete = function(rec, blob: Blob) {
                audioReady(blob, "Recorded audio (" + blob.size + " bytes)");
                $('#recordmic').prop('disabled', false);
                $('#recordstate').text('\u00A0');
                progressbar.close();
            };
            recorder.onTimeout = stopRecording;
            recorder.setOptions({
                timeLimit:20,
                encodeAfterRecord:true,
                mp3: {bitRate: 128},
            });
            $('#recordstate').text('Recording...');
            recorder.startRecording();
        }).catch(function(err) {
            // couldn't get mic access, user may have refused
            console.log(err);
            $('#recordmic .mdc-button__icon').text('mic_off');
            $('#recordmic .mdc-button__label').text('Mic not available');
            $('#recordmic').prop('disabled', true);
        });
    });

    // set up the waveform
    surfer = WaveSurfer.create({
        container: '#input-waveform',
        height: 40,
        backend: 'MediaElement',
    });

    surfer.on('ready', function() {
        $('#control-input').prop('disabled', false);
        const filedata = $('#filedata');
        filedata.text(filedata.text() + ' - ' + surfer.getDuration() + " seconds");
    });

    surfer.on('play', function() {
        $('#control-input').attr('aria-label', 'pause');
        $('#control-input i').text('pause');
    });

    surfer.on('pause', function() {
        $('#control-input').attr('aria-label', 'play');
        $('#control-input i').text('play_arrow');
    });

    $('#control-input').on('click', function(e) {
        e.preventDefault();
        surfer.playPause();
    });

    $("#submit-form").on('click', function (e) {
        e.preventDefault();

        // disable the button until the REST call returns
        $('#submit-form').prop('disabled', true);
        $('#elg-messages').empty();

        elgCommon.callService(currentBlob, curBlobContentType, handleResponse);
        return false;
    });
}

export function curBlob() {
    return currentBlob;
}

export function blobUrl() {
    if(objectUrl == null && currentBlob != null) {
        objectUrl = URL.createObjectURL(currentBlob);
    }
    return objectUrl;
}