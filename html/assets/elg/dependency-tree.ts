import type ElgCommon = require("./common");
import * as $ from "jquery";

// @ts-ignore
import treexView = require('treex-view');
const treex = treexView; // to prevent typescript optimizer from removing the import
import {makeTexts} from "./annotations-or-texts";
import type {ElgAnnotation, ElgFeatures, ElgText} from "data/elgText";
import {flattenTokens} from "data/elgText";

// lots of config options that come from the query string

/**
 * Annotation type representing tokens.  Note that we follow the same role-based logic to flatten a list of
 * texts with role "word" or "token" into a leaf node with each item covered by an annotation of type
 * token/word as appropriate, so for responses of this kind specify ?token=token or ?token=word.
 */
let tokenAnnType = "token";

/**
 * Annotation type denoting sentences.  If unset, each leaf text in the texts tree (or the whole input in
 * the case of an annotations response) is treated as a single sentence.
 */
let sentenceAnnType: string;

/**
 * If the response permits multi-word tokens, we take the list of words from this named feature in each token.
 * If unset, each token is treated as one word and everything else is taken directly from the annotation features.
 * Thus for one-word-per-token (without a ?words=... parameter):
 *
 * {
 *     "start": 0, "end":4, "features":{"parent":"node2", "pos":"NN"}
 * }
 *
 * but with ?words=wd then we expect
 *
 * {
 *     "start": 0, "end":4, "features":{
 *          "wd":[
 *              {"parent":"node2", "pos":"NN"}
 *          ]
 *     }
 * }
 */
let wordsFeature: string;

/**
 * Feature name in which we can find the word form.  If unset, the text under the token annotation is used directly.
 */
let formFeature: string;

/**
 * Feature name in which we can find the ID of the word, to which parent links will point.  If unset we synthesize an
 * ID from a one-based index of the word in the sentence, and assume the parent links are compatible one-based numbers.
 */
let idFeature: string = "id";

/**
 * Feature name giving the link from one word to its parent in the dependency tree.  Default is "parent" if unset.
 * The value of the parent feature must match the ID of another word in the sentence, or a one-based or zero-based
 * index to the parent word (depending on the "first" parameter) if the words do not have explicit IDs.
 */
let parentFeature: string = "parent";

/**
 * Index that is considered to be the first word in the sentence.  Defaults to 1 but can be overridden to 0 for
 * zero-based indexes.
 */
let firstWord: number = 1;

/**
 * Expressions defining how to build the node labels in the rendered tree.  May be specified multiple times to
 * produce multi-line labels.  Each label parameter should be either "text" (meaning the text of the word - either
 * the text covered by the token annotation or the value of the specified form feature), or the name of a feature
 * (such as a part-of-speech tag or dependency relation label).  The value may be optionally prefixed with a six
 * digit hex colour code and semicolon, e.g. "?label=text&label=00ff00;upos" would display two lines, one with the
 * text in black and one with the "upos" feature in green.
 */
let labels: string[] = ["text"];

const myUrl = new URL(window.location.href);
// preset formats

/**
 * Since there are many combinations of parameters, we provide convenient presets for common groups of services.
 * <dl>
 *     <dt>udpipe</dt>
 *     <dd>token=udpipe/tokens, sentence=udpipe/sentences, words=words, form=form, numeric word IDs, parent=head,
 *     label with the word form, deprel and upos.</dd>
 * </dl>
 */
const preset = myUrl.searchParams.get("preset");
if(preset) {
    switch(preset) {
        case 'udpipe':
            tokenAnnType = "udpipe/tokens";
            sentenceAnnType = "udpipe/sentences";
            wordsFeature = "words";
            formFeature = "form";
            idFeature = "";
            parentFeature = "head";
            labels = ["form", "00008b;deprel", "004048;upos"];
            break;
        default:
            // do nothing
    }
}
// individual parameters
if(myUrl.searchParams.has("token")) {
    tokenAnnType = myUrl.searchParams.get("token");
}
if(myUrl.searchParams.has("sentence")) {
    sentenceAnnType = myUrl.searchParams.get("sentence");
}
if(myUrl.searchParams.has("words")) {
    wordsFeature = myUrl.searchParams.get("words");
}
if(myUrl.searchParams.has("form")) {
    formFeature = myUrl.searchParams.get("form");
}
if(myUrl.searchParams.has("id")) {
    idFeature = myUrl.searchParams.get("id");
}
if(myUrl.searchParams.has("parent")) {
    parentFeature = myUrl.searchParams.get("parent");
}
if(myUrl.searchParams.has("first")) {
    firstWord = parseInt(myUrl.searchParams.get("first"));
}
if(myUrl.searchParams.has("label")) {
    labels = myUrl.searchParams.getAll("label");
}


interface Token {
    text: string;
    id: string;
    spaceBefore: boolean;
    features: ElgFeatures;
}

interface Sentence {
    tokens: Token[];
    idMap: {[idFeature: string | number] : string};
}

interface TreeNode {
    id: string;
    order: number;
    parent: string | null;
    data: any;
    labels: string[];
    // links to other nodes
    firstson?: string;
    rbrother?: string;
}

function buildSentences(text: ElgText, depth: number): Sentence[] {
    text = flattenTokens(text, depth);
    let sentences: Sentence[] = [];
    if(text.content !== undefined) {
        let sentenceOffsets: number[][] = [];
        if(sentenceAnnType && text.annotations && text.annotations[sentenceAnnType]?.length) {
            // use sentence annotations
            for(let s of text.annotations[sentenceAnnType].sort((a, b) => a.start - b.start)) {
                sentenceOffsets.push([s.start, s.end]);
            }
        } else {
            // treat the whole text as one sentence
            sentenceOffsets.push([0, text.content.length]);
        }
        for(let [start, end] of sentenceOffsets) {
            const tokens: ElgAnnotation[] = [];
            if(text.annotations && tokenAnnType in text.annotations && text.annotations[tokenAnnType].length) {
                for(let tok of text.annotations[tokenAnnType]) {
                    if(tok.start >= start && tok.end <= end) {
                        tokens.push(tok);
                    }
                }
            }
            tokens.sort((a, b) => a.start - b.start);
            let sentenceWords: Token[] = [];
            let idMap: {[idFeature: string | number] : string} = {};
            let i = 0;
            let lastEnd: number = -1;
            for(let ann of tokens) {
                let word: ElgFeatures;
                for(word of (wordsFeature ? ann.features[wordsFeature] : [ann.features])) {
                    let wordId = word[idFeature];
                    if(typeof wordId === 'undefined') {
                        // no explicit ID feature, so use the word number (counting from firstWord)
                        wordId = i + firstWord;
                    }
                    idMap[wordId] = `elg_w${i}`;
                    sentenceWords.push({
                        text: (formFeature && formFeature in word ? (word[formFeature] as string) : text.content.substring(ann.start, ann.end)),
                        id: wordId,
                        // put space before this word if either (a) we're allowing MWTs or (b) this token
                        // starts strictly after the previous one ended, not at exactly the same place
                        spaceBefore: !!(wordsFeature || lastEnd >= 0 && ann.start > lastEnd),
                        features: word,
                    });
                    i++;
                }
                lastEnd = ann.end;
            }
            sentences.push({tokens: sentenceWords, idMap: idMap});
        }
    } else {
        // this is a branch node, recurse
        for(let t of text.texts) {
            const childSentences = buildSentences(t, depth+1)
            sentences.push(...childSentences);
        }
    }
    return sentences;
}

function parentOf(token: Token, sentence: Sentence): string {
    let parentFeatureVal: any = null;
    if(token.features && parentFeature in token.features) {
        parentFeatureVal = token.features[parentFeature];
    }
    if(typeof parentFeatureVal === 'undefined' || parentFeatureVal === null) {
        // no parent - use the root node
        return "elg_root0";
    } else {
        let target: Token;
        for(let t of sentence.tokens) {
            if(t.id == parentFeatureVal) { // == not === is deliberate, to allow id 1 to match parent "1" or v/v
                target = t;
                break;
            }
        }
        if(target) {
            return sentence.idMap[target.id];
        } else {
            // couldn't find target, so recover by linking to the root
            return "elg_root0";
        }
    }
}

function labelsFor(token: Token): string[] {
    let l = [];
    for(let item of labels) {
        let split = item.split(';', 2);
        let colour = "";
        if(split.length > 1) {
            colour = `#{#${split[0]}}`;
            item = split[1];
        }
        if(item === "text") {
            l.push(colour + (token.text || " "));
        } else {
            l.push(colour + (token.features[item] || " "));
        }
    }
    return l;
}

export function responseHandler(elgCommon: ElgCommon): (data: any) => void {
    $('#test-again').on('click', (e) => {
        e.preventDefault();
        $('#elg-annotate-result').addClass('hidden');
        $('#elg-test-form').removeClass('hidden');
    });

    function handleResponse(data: any) {
        if (data.response && data.response.hasOwnProperty("type")
            && (data.response.type === 'annotations' || data.response.type === 'texts')) {

            $('#elg-test-form').addClass('hidden');
            $('#elg-annotate-result').removeClass('hidden');

            // re-enable the button
            $('#submit-form').prop('disabled', false);

            try {
                let elgTexts = makeTexts(data);

                // construct a list of sentences, where each sentence is a list of tokens.
                // We assume that the leaf nodes in the elgTexts tree (either genuine leaf
                // nodes or flattened list-of-tokens branch nodes) are the initial sentences,
                // unless we have been configured with a sentence annotation type in which
                // case we look for spans covered by annotations of that type within the
                // leaf nodes.
                let sentences = buildSentences({texts: elgTexts}, 0);

                const trees: any[] = [];
                for (let sentence of sentences) {
                    let treeDesc: [string, string][] = [];
                    let i = 0;
                    const rootLabels = Array(Math.max(labels.length, 1));
                    rootLabels[0] = "<root>";
                    rootLabels.fill("", 1);
                    let treeNodes: TreeNode[] = [{
                        id: 'elg_root0',
                        order: i++,
                        parent: null,
                        data: {form: "<root>"},
                        labels: rootLabels,
                    }];
                    const nodesById: { [id: string]: TreeNode } = {
                        elg_root0: treeNodes[0],
                    };
                    for (let token of sentence.tokens) {
                        if (treeDesc.length && token.spaceBefore) treeDesc.push([' ', 'space']);
                        const node = {
                            id: sentence.idMap[token.id],
                            order: i++,
                            data: token.features,
                            labels: labelsFor(token),
                            parent: parentOf(token, sentence),
                        };
                        treeNodes.push(node);
                        treeDesc.push([token.text, node.id]);
                        nodesById[sentence.idMap[token.id]] = node;
                    }

                    // check for cycles
                    for(let node of treeNodes) {
                        const seen: string[] = [];
                        while(node.parent) {
                            if(seen.indexOf(node.id) >= 0) {
                                // cyclic dependency - break the cycle by detaching the last node that pointed to me
                                // and re-parent it to the root
                                const cycleNode = nodesById[seen[seen.length-1]];
                                cycleNode.parent = "elg_root0";
                                cycleNode.labels.push("(*)");
                                break;
                            }
                            seen.push(node.id);
                            node = nodesById[node.parent];
                        }
                    }

                    // add downward links
                    const lastChild: { [id: string]: TreeNode } = {};
                    for (let i = 1; i < treeNodes.length; i++) {
                        const node = treeNodes[i];
                        const lastOfParent = lastChild[node.parent];
                        if (lastOfParent) {
                            // already a child list for this parent, so append to it
                            lastOfParent.rbrother = node.id;
                        } else {
                            // start child list for this parent
                            nodesById[node.parent].firstson = node.id;
                        }
                        lastChild[node.parent] = node;
                    }

                    trees.push({desc: treeDesc, zones: {conllu: {trees: {"a": {layer: "a", nodes: treeNodes}}}}});
                }
                // display the trees
                $('#treeView').treexView(trees);
            } catch(e) {
                console.log(e);
                const tv = $('#treeView');
                tv.empty();
                tv.append($('<em>Unable to build tree:</em>'));
                tv.append($('<span></span>').text(e.toString()));
            }
        } else {
            const msgsContainer = $('#elg-messages');
            let error: ElgStatus;
            if(data.response && data.response.type) {
                error = {
                    code:'elg.response.type.unsupported',
                    text:'Response type {0} not supported',
                    params:[data.response.type]
                }
            } else {
                error = {
                    code:'elg.response.invalid',
                    text:'Invalid response message'
                }
            }
            elgCommon.resolveErrors([error]).then((resolvedErrors) => {
                $('<div class="alert alert-warning"></div>').text(resolvedErrors[0]).appendTo(msgsContainer);
                // re-enable the button
                $('#submit-form').prop('disabled', false);
            });
        }

    }

    return handleResponse;
}