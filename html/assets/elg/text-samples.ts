import * as $ from "jquery";
import ElgCommon = require("./common");
import sampleFetcher = require("./sample-fetcher");

/**
 * Function to process the sample text as retrieved from the metadata and
 * display it appropriately in the specified sample card.
 *
 * @param elt jQuery object with the element into which the sample text
 *              should go
 * @param sample the sample to format
 */
type SampleFormatFunction = (sample: ElgSample, elt: JQuery, elgCommon: ElgCommon) => void;

/**
 * Function to process the sample text as retrieved from the metadata and
 * populate the test form field or fields appropriately.
 *
 * @param elt jQuery object - the <code>textField</code> that was passed
 *             to metadataHandler.
 * @param sample the sample to insert into the form
 */
type SampleInsertFunction = (sample: ElgSample, elt: JQuery, elgCommon: ElgCommon) => void;

/**
 * Default SampleFormatFunction - just sets the given content as the
 * element text, unless it's a JSON request in which case extract the
 * "content" property and use that.
 */
function defaultSampleFormatter(sample: ElgSample, elt: JQuery, _: ElgCommon): void {
    let text = sample.sample_text;
    try {
        const jsonSample = JSON.parse(text);
        if(jsonSample.content) {
            text = jsonSample.content;
        }
    } catch(e) {
        // couldn't parse as JSON, that's fine, it's probably just text.
    }

    elt.text(text);
}

/**
 * Default SampleInsertFunction - uses MDCTextField API to set the sample text
 * as the content of the given element (which is assumed to be an MDC text field).
 */
function defaultSampleInserter(sample: ElgSample, elt: JQuery, common: ElgCommon): void {
    let text = sample.sample_text;
    try {
        const jsonSample = JSON.parse(text);
        if(jsonSample.content) {
            text = jsonSample.content;
        }
        if(typeof jsonSample.params === 'object') {
            common.setParams(jsonSample.params);
        }
    } catch(e) {
        // couldn't parse as JSON, that's fine, it's probably just text.
    }
    (elt.closest('.mdc-text-field')[0] as any).MDCTextField.value = text;
}

/**
 * Inspect the list of service functions returned in the metadata and change
 * the wording of the text input field label if necessary to make it more
 * appropriate to the function.
 * @param label jQuery object referencing the text field label span
 * @param resultLabel jQuery object referencing the heading above the output
 * @param functions the list of service functions from the metadata
 */
function fixUpTextFieldLabel(label: JQuery, resultLabel: JQuery, functions: string[]): void {
    if($.inArray("http://w3id.org/meta-share/omtd-share/Summarization", functions) >= 0) {
        label.text("Type text to summarize");
        resultLabel.text("Summary");
    } else if($.inArray("http://w3id.org/meta-share/omtd-share/Parsing", functions) >= 0 ||
        $.inArray("http://w3id.org/meta-share/omtd-share/ConstituencyParsing", functions) >= 0 ||
        $.inArray("http://w3id.org/meta-share/omtd-share/DeepParsing", functions) >= 0 ||
        $.inArray("http://w3id.org/meta-share/omtd-share/DependencyParsing", functions) >= 0 ||
        $.inArray("http://w3id.org/meta-share/omtd-share/FrameSemanticParsing", functions) >= 0) {
        label.text("Type text to parse");
        resultLabel.text("Parsed");
    } else if($.inArray("http://w3id.org/meta-share/omtd-share/LexicalSimplification", functions) >= 0 ||
        $.inArray("http://w3id.org/meta-share/omtd-share/TextSimplification", functions) >= 0) {
        label.text("Type text to simplify");
        resultLabel.text("Simplified");
    } else if($.inArray("http://w3id.org/meta-share/omtd-share/Anonymization", functions) >= 0 ||
        $.inArray("http://w3id.org/meta-share/omtd-share/Pseudonymization", functions) >= 0 ||
        $.inArray("http://w3id.org/meta-share/omtd-share/TextEncryption", functions) >= 0) {
        label.text("Type text to anonymize");
        resultLabel.text("Processed");
    } else if($.inArray("http://w3id.org/meta-share/omtd-share/GrammarChecking", functions) >= 0 ||
        $.inArray("http://w3id.org/meta-share/omtd-share/LanguageChecking", functions) >= 0 ||
        $.inArray("http://w3id.org/meta-share/omtd-share/SpellChecking", functions) >= 0) {
        label.text("Type text to check");
        resultLabel.text("Checked");
    } else if($.inArray("http://w3id.org/meta-share/omtd-share/TextCategorization", functions) >= 0) {
        label.text("Type text to categorize");
        resultLabel.text("Result");
    } // else do nothing, stay with the default for this GUI type (annotate/translate/classify)
}

/**
 * Returns a function which can be used as a metadata callback with ElgCommon.
 * The function will extract textual samples from the service metadata and
 * create a suitable user interface to allow the user to select one of the samples
 * and insert its content into the text box for submission to the remote service.
 * @param textField jQuery object referencing the text field of the submission form,
 * such that <code>textField.val(sampleText)</code> will fill in the form with the
 * sample.
 * @param resultLabel jQuery object (possibly empty) referencing a heading describing
 * the nature of the result from this service.
 * @param target jQuery object referencing the container element (typically a div)
 * where the sample selection UI should be put.
 * @param toShow jQuery object referencing any hidden elements that should be shown
 * if there are any valid samples.
 * @param sampleFormatter function to customize the way sample data is formatted in
 * the sample card.  By default the sample text content is simply inserted as-is
 * @param sampleInserter function to customize the way sample data is used to populate
 * the form fields.  By default the sample text content is set as the value of
 * <code>textField</code> using the MDC JavaScript API.
 * @returns function suitable to be passed as the metadataCallback argument to
 * ElgCommon.
 */
function metadataHandler(textField: JQuery, resultLabel: JQuery, target: JQuery, toShow: JQuery,
                         sampleFormatter: SampleFormatFunction = defaultSampleFormatter,
                         sampleInserter: SampleInsertFunction = defaultSampleInserter) {
    const msgsContainer = $('#elg-messages');

    function populateSamples(metadata: any, elgCommon: ElgCommon) {
        if (metadata.described_entity &&
            metadata.described_entity.lr_subclass &&
            metadata.described_entity.lr_subclass.function &&
            metadata.described_entity.lr_subclass.function.length) {
            fixUpTextFieldLabel(textField.closest('.mdc-text-field').find('.mdc-floating-label'),
                resultLabel, metadata.described_entity.lr_subclass.function);
        }
        const validSamples: ElgSample[] = [];
        // we want to extract all samples from all input resource types that are
        // either "user input text" or a "file" of the "text" media type.
        if (metadata.described_entity &&
            metadata.described_entity.lr_subclass &&
            metadata.described_entity.lr_subclass.input_content_resource) {
            $.each(metadata.described_entity.lr_subclass.input_content_resource, function (i, icr) {
                if (icr.processing_resource_type === 'http://w3id.org/meta-share/meta-share/userInputText'
                    || (icr.processing_resource_type === 'http://w3id.org/meta-share/meta-share/file1'
                        && icr.media_type === 'http://w3id.org/meta-share/meta-share/text')) {
                    $.each(icr.sample, function (j, sam: ElgSample) {
                        validSamples.push(sam);
                    });
                }
            });
        }

        if (validSamples.length) {
            // we have some valid samples
            sampleFetcher('config/sample-helpers.json').then(function (fetcher) {
                // make a card for each sample
                for (const s of validSamples) {
                    const card = $('<section class="mdc-card sample-card"></section>');
                    const content = $('<div class="mdc-card__primary-action" data-mdc-auto-init="MDCRipple"></div>').appendTo(card);
                    if (s.tag) {
                        content.append($('<h2 class="mdc-typography--subtitle2 sample-tag"></h2>').text(s.tag));
                    }
                    const sampleTextElt = $('<p class="mdc-typography--body2 sample-text"></p>');
                    if (s.sample_text) {
                        sampleFormatter(s, sampleTextElt, elgCommon);
                    } else {
                        sampleTextElt.html("<em>Click to download</em>");
                    }
                    content.append(sampleTextElt);
                    s.textElement = sampleTextElt;
                    card.data('elg-sample', s);
                    target.append(card);
                }

                // set up event handlers
                $('#sample-spinner-holder, #samples-overlay').on('click', (e) => {
                    // modal overlays - ignore clicks
                    e.preventDefault();
                });
                target.on('click', '.sample-card', function(e) {
                    e.preventDefault();
                    const sample: ElgSample = $(this).data('elg-sample');
                    if (sample.sample_text) {
                        sampleInserter(sample, textField, elgCommon);
                    } else {
                        // remote sample at a URL
                        textField.prop('disabled', true);
                        $('.sample-spinner-holder, #samples-overlay').removeClass('hidden');
                        $('.sample-spinner').each((i, e) => (e as any).MDCCircularProgress.open());

                        fetcher.fetchSample('text', sample.samples_location).done((data: string) => {
                            // cache content so we don't have to fetch it again
                            sample.sample_text = data;
                            sampleFormatter(sample, sample.textElement, elgCommon);
                            sampleInserter(sample, textField, elgCommon);
                        }).fail(function (errors: ElgStatus[]) {
                            elgCommon.resolveErrors(errors).then((data) => {
                                $.each(data, (i, msg) => {
                                    msgsContainer.append($('<div class="alert alert-warning"></div>').text(msg));
                                });
                            });
                        }).always(function () {
                            textField.prop('disabled', false);
                            $('.sample-spinner-holder, #samples-overlay').addClass('hidden');
                            $('.sample-spinner').each((i, e) => (e as any).MDCCircularProgress.close());
                        });
                    }
                });
                toShow.removeClass("hidden");
            });
        }
    }

    return populateSamples;
}

export = metadataHandler;
