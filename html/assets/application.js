// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require lib/require
//= require_self

requirejs.config({ //this initiates the configuration
    baseUrl: 'assets/',
    bundles: {
        "annotation-viewer":["annotation-viewer", "data/annotationSelector", "data/elgText", "views/selector"]
    },
    shim: {
        'treex-view': ['jquery'],
    },
    paths: {
        jquery: 'lib/jquery-2.2.0.min',
        'annotation-viewer': 'annotation-viewer/bundle',
        underscore: 'lib/underscore',
        bootstrap: 'lib/bootstrap',
        'treex-view': 'lib/js-treex-view.min',
        "material-components-web":"lib/material-components-web-14.0.0.min",
    }
});

(function() {
    var fontCss = document.getElementById("font-css");
    if(!fontCss) {
        fontCss = document.createElement("link");
        fontCss.id = "font-css";
        fontCss.rel = "stylesheet";
        // use the self-hosted version on ELG, point to CDN when this UI is being used for
        // an "elg local-installation"
        if(window.location.origin.includes("european-language-grid.eu")) {
            fontCss.href = "/catalogue/fonts.css";
        } else {
            fontCss.href = "https://fonts.googleapis.com/css?family=Noto+Sans&display=swap";
        }
        document.head.appendChild(fontCss);
    }
})();

//
// if (typeof jQuery !== 'undefined') {
//     (function($) {
//         $(document).ajaxStart(function() {
//             $('#spinner').fadeIn();
//         }).ajaxStop(function() {
//             $('#spinner').fadeOut();
//         });
//     })(jQuery);
// }
//
// if (!Object.keys) {
//     Object.keys = function (obj) {
//         var keys = [],
//             k;
//         for (k in obj) {
//             if (Object.prototype.hasOwnProperty.call(obj, k)) {
//                 keys.push(k);
//             }
//         }
//         return keys;
//     };
// }
