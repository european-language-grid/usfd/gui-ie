/**
 * Created by Ian Roberts, 16/04/2020, based on work by Dominic Rout on 09/12/2016.
 */

export class Annotation {
    private _type : string;
    private _startOffset : number;
    private _endOffset : number;
    private _sourceStart : number;
    private _sourceEnd : number;
    private _features : Map<any, any>;


    constructor(type: string, startOffset: number,
                endOffset: number, features = null,
                sourceStart: number = null, sourceEnd: number = null) {
        this._type = type;
        this._startOffset = startOffset;
        this._endOffset = endOffset;
        this._features = features;
        this._sourceStart = sourceStart;
        this._sourceEnd = sourceEnd;
    }

    get type(): string {
        return this._type;
    }

    get startOffset(): number {
        return this._startOffset;
    }

    get endOffset(): number {
        return this._endOffset;
    }

    get features(): Map<any, any> {
        return this._features;
    }

    get sourceStart(): number {
        return this._sourceStart;
    }

    get sourceEnd(): number {
        return this._sourceEnd;
    }
}