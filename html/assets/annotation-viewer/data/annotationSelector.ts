
import * as $ from "jquery";
import {Colour} from "./colour";
var colours;

colours = [[31, 119, 180], [174, 199, 232], [255, 127, 14], [255, 187, 120], [44, 160, 44], [152, 223, 138],
    [214, 39, 40], [255, 152, 150], [148, 103, 189], [197, 176, 213], [140, 86, 75], [196, 156, 148],
    [227, 119, 194], [247, 182, 210], [127, 127, 127], [199, 199, 199], [188, 189, 34],
    [219, 219, 141], [23, 190, 207], [158, 218, 229]];

/**
 * Manages the list of annotation types and their colours.
 */
export class AnnotationSelector {
    private typeColours: Map<string, Colour>;
    private lastColourUsed : number;
    private isVisible: Map<string, boolean>;

    constructor() {
        this.lastColourUsed = 0;
        this.typeColours = new Map<string, Colour>();
        this.isVisible = new Map<string, boolean>();
    }

    /**
     * Generates a colour for the given type, or retrieves it if there already is one.
     * @param type Annotation Type of interest
     * @param alpha
     * @returns {string|T[]|any|number[]}
     */
    public typeColour(type : string, alpha : number = 0.3) {
        if (this.typeColours.has(type)) {
            return this.typeColours.get(type).withAlpha(alpha);
        } else {
            // Get the next colour to use.
            const colour = Colour.fromRGB(colours[this.lastColourUsed]);
            this.lastColourUsed += 1;
            if (this.lastColourUsed === colours.length) {
                this.lastColourUsed = 0;
            }

            this.typeColours.set(type, colour);

            return colour;
        }
    }

    public addType(type: string, visible: boolean = true) {
        this.typeColour(type); // for side effects
        this.isVisible.set(type, visible);
    }

    get knownTypes() {
        return this.typeColours.keys();
    }

    public isAnnotationVisible(type: string) {
        return this.isVisible.has(type) && this.isVisible.get(type);
    }

    /**
     * Update the list of visible annotation types
     */
    public visibleTypes(types: Array<string>) {
        const oldVisible = this.isVisible;
        const newVisible = new Map<string, boolean>();
        oldVisible.forEach((value, type) => {
            newVisible.set(type, false);
        });
        types.forEach((type) => {
            newVisible.set(type, true);
        });
        this.isVisible = newVisible;
        $(this).triggerHandler("visible:changed");
    }
}