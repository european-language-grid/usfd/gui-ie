/*
//	Encodes the colour values at each position of the document text.
*/

import {Colour} from "./colour";
import {Annotation} from "./annotation";
import * as $ from "jquery";
import {Segment} from "./segment";

export type BitMap = Array<Colour>;
export type AnnotationMap = Array<Array<Annotation>>;


export class ColourField {
    private colourField : BitMap;
    private annotationsAt : AnnotationMap;
    private segment : Segment;

    constructor(segment: Segment) {
        this.segment = segment;
        this.colourField = new Array<Colour>(segment.text.length);
        this.colourField.fill(new Colour(255,255,255,0))

        this.annotationsAt = new Array<Array<Annotation>>(segment.text.length);
        this.annotationsAt.fill([]);

        $(segment.annotationSelector).on("visible:changed", (event, data) => {
                this.update();
            }
        );

        this.update();
    }

    public update() {
        const start = 0;
        const end = this.colourField.length;

        const visible = this.segment.visibleAnnotations;

        // First, reset the colour field and annotations lists.
        for (let i = start; i < end; i++) {
            this.colourField[i] = new Colour(255,255,255,0);
            this.annotationsAt[i] = [];
        }


        // Fill in the colour for each individual annotation.
        visible.forEach((annotation : Annotation) => {
            let startOffset = annotation.startOffset;
            let endOffset = annotation.endOffset;

            // Only fill annotations within the specified range. Search is currently very slow. Sort the annotations?
            if (startOffset < end && endOffset > start) {
                startOffset = Math.max(startOffset, start);
                endOffset = Math.min(endOffset, end);

                for (let i = startOffset; i < endOffset; i++) {
                    const annotationColour = this.segment.annotationSelector.typeColour(annotation.type);
                    this.colourField[i] = this.colourField[i].combineAlpha(annotationColour);
                    this.annotationsAt[i].push(annotation);
                }
            }
        });
        $(this).triggerHandler("changed", {start: start, end:end});
    }

    public get(offset : number) : Colour {
        return this.colourField[offset];
    }

    public getAnnotations(offset: number) : Array<Annotation> {
        return this.annotationsAt[offset];
    }
}

