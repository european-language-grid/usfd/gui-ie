// interface mappings for the ELG response types

import * as $ from "jquery";

export interface ElgFeatures {
    [key: string]: any;
}

export interface ElgAnnotation {
    start?: number;
    end?: number;
    sourceStart?: number;
    sourceEnd?: number;
    features?: ElgFeatures;
}

export interface ElgAnnotations {
    [type:string]: Array<ElgAnnotation>;
}

export interface ElgText {

    content?: string;

    annotations?: ElgAnnotations;

    features?: ElgFeatures;

    texts?: Array<ElgText>;

    role?: string;

    score?: number;
}

/**
 * Global variable expected to be populated by the host page with the list of default "role"
 * values for each level of the expected texts response.  The last entry in the array is used
 * as the default for any levels deeper than the length of the array, and if no array is
 * provided at all then "alternative" is the default.
 */
declare var elgDefaultRoles: string[];

export function roleFor(text: ElgText, depth: number) {
    if(text.role) {
        return text.role;
    } else if(typeof elgDefaultRoles !== 'undefined' && elgDefaultRoles.length > 0) {
        if(depth < elgDefaultRoles.length) {
            return elgDefaultRoles[depth];
        } else {
            return elgDefaultRoles[elgDefaultRoles.length - 1];
        }
    } else {
        return "alternative";
    }
}

function annotations(anns: ElgAnnotations, type: string) : Array<ElgAnnotation> {
    let annList = anns[type];
    if(typeof annList === 'undefined') {
        annList = [];
        anns[type] = annList;
    }
    return annList;
}

export function flattenTokens(text: ElgText, depth: number): ElgText {
    if(text.content !== undefined) {
        return text;
    }
    const firstTextRole = roleFor(text.texts[0], depth);
    if((firstTextRole === "token" || firstTextRole === "word")
        && text.texts.every((t) => t.content !== undefined)) {
        // list of leaf node tokens, transform into a single leaf node with annotations
        const tokenStarts: number[] = [];
        const tokenEnds: number[] = [];
        const synthText: ElgText = {
            content: "",
            role: "segment",
            annotations: {},
        };
        let off = 0;
        for(const tok of text.texts) {
            if(off > 0) {
                synthText.content += ' ';
                off++;
            }
            const tokAnn: ElgAnnotation = {
                features: tok.features
            };
            tokenStarts.push(off);
            tokAnn.start = off;
            synthText.content += tok.content;
            off += tok.content.length;
            tokenEnds.push(off);
            tokAnn.end = off;

            annotations(synthText.annotations, roleFor(tok, depth)).push(tokAnn);
        }

        // deal with any other annotations
        if(text.annotations && !$.isEmptyObject(text.annotations)) {
            for(let type in text.annotations) {
                if(text.annotations.hasOwnProperty(type)) {
                    for(let ann of text.annotations[type]) {
                        if(ann.start < tokenStarts.length && ann.end <= tokenEnds.length) {
                            // this annotation falls within the list of tokens, map it
                            // to the corresponding offsets in the synthText
                            let newAnn: ElgAnnotation = {
                                start: tokenStarts[ann.start],
                                end: tokenEnds[ann.end - 1],
                                features: ann.features,
                            }
                            if(typeof ann.sourceStart !== 'undefined') {
                                newAnn.sourceStart = ann.sourceStart;
                            }
                            if(typeof ann.sourceEnd !== 'undefined') {
                                newAnn.sourceEnd = ann.sourceEnd;
                            }
                            annotations(synthText.annotations, type).push(newAnn);
                        }
                    }
                }
            }
        }

        return synthText;
    } else {
        // not a leaf node or list-of-tokens - return as-is
        return text;
    }
}