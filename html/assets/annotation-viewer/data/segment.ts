import {Annotation} from "./annotation";
import {AnnotationSelector} from "./annotationSelector";
import {ColourField} from "./colourField";
import {ElgAnnotation, ElgAnnotations} from "./elgText";

/**
 * Global variable which can be populated by the host page if only
 * specific annotation types should be visible by default.  If empty
 * or unset, all annotation types are visible by default unless
 * hidden by elgDefaultHiddenTypes.
 */
declare var elgDefaultShownTypes: string[];

/**
 * Global variable which can be populated by the host page if
 * certain annotation types should not be visible by default.  If
 * empty or unset then all types are selected by default.  The logic
 * is that a given type T is shown by default if
 *
 * (elgDefaultShownTypes-is-undef-or-empty || elgDefaultShownTypes.includes(T)) &&
 * (elgDefaultHiddenTypes-is-undef-or-empty || !elgDefaultHiddenTypes.includes(T))
 */
declare var elgDefaultHiddenTypes: string[];

/**
 * Determines whether or not a given annotation type should be selected by default.
 * @param type the annotation type
 */
function shouldShow(type: string): boolean {
    return (
        typeof elgDefaultShownTypes === 'undefined' ||
            elgDefaultShownTypes.length === 0 ||
            elgDefaultShownTypes.indexOf(type) >= 0)
        && (typeof elgDefaultHiddenTypes === 'undefined' ||
            elgDefaultHiddenTypes.indexOf(type) < 0)
}

/**
 * Represents a single segment of text with associated annotations.
 */
export class Segment {
    private _text: string;
    private _annotations: Map<string, Array<Annotation>>;
    private _annotationSelector: AnnotationSelector;
    private _colourField: ColourField;

    constructor(text: string, annotations: ElgAnnotations, annotationSelector: AnnotationSelector) {
        this._text = text;
        this._annotationSelector = annotationSelector;
        this._annotations = new Map<string, Array<Annotation>>();

        // ELG responses count offsets in Unicode characters, JavaScript counts in
        // UTF-16, so we must calculate correction factors for supplementary characters.
        // Essentially we add one to the correction at each point where the *preceding*
        // character was a supplementary.
        const corrections = [[0,0]];
        let cpOffset = 0;
        let charOffset = 0;
        while(charOffset < text.length) {
            cpOffset++;
            if(charOffset + 1 < text.length) {
                var possHighSurrogate = text.charCodeAt(charOffset);
                var possLowSurrogate = text.charCodeAt(charOffset+1);
                if(possHighSurrogate >= 0xD800 && possHighSurrogate <= 0xDBFF
                    && possLowSurrogate >= 0xDC00 && possLowSurrogate <= 0xDFFF) {
                    // this is a valid surrogate pair, so add a correction
                    charOffset += 2;
                    corrections.push([cpOffset, (charOffset-cpOffset)]);
                    continue;
                }
            }
            // if we get to here we're not looking at a supplementary (either
            // this char is not a surrogate, or it is but it's unpaired)
            charOffset++;
        }

        const reposition = (offset: number) => {
            let i = 1;
            while(i < corrections.length && corrections[i][0] <= offset) {
                i++;
            }
            // the appropriate correction is the one we just passed
            return offset + corrections[i-1][1];
        };

        for(const type in annotations) {
            if(annotations.hasOwnProperty(type)) {
                const annsList: Array<ElgAnnotation> = annotations[type];
                if(annsList.length) {
                    this._annotationSelector.addType(type, shouldShow(type));
                    const annsOfType: Array<Annotation> = [];
                    this._annotations.set(type, annsOfType);
                    annsList.forEach((annObj) => {
                        const sourceStart: number = (annObj.hasOwnProperty('sourceStart') ? annObj.sourceStart : null);
                        const sourceEnd: number = (annObj.hasOwnProperty('sourceEnd') ? annObj.sourceEnd : null);
                        annsOfType.push(new Annotation(type, reposition(annObj.start), reposition(annObj.end), annObj.features,
                            sourceStart, sourceEnd));
                    });
                }
            }
        }

        this._colourField = new ColourField(this);
    }

    get text(): string {
        return this._text;
    }

    get annotationSelector(): AnnotationSelector {
        return this._annotationSelector;
    }

    get colourField() : ColourField {
        return this._colourField;
    }


    /**
     * Produces a flat list of annotations that should be displayed in the document.
     * @returns {any}
     */
    get visibleAnnotations(): Array<Annotation> {
        const result: Annotation[] = [];
        this._annotations.forEach((annotationsOfType, type) => {
            if(this._annotationSelector.isAnnotationVisible(type)) {
                annotationsOfType.forEach((ann: Annotation) => {
                    result.push(ann);
                })
            }
        });

        return result;
    }

}