import {Segment} from "./data/segment";
import {AnnotationSelector} from "./data/annotationSelector";
import {ColourFieldView} from "./views/colourFieldView";
import {Selector} from "./views/selector";
import {Viewer} from "./views/viewer";
import {ElgAnnotations, ElgText, roleFor, flattenTokens, ElgAnnotation} from "./data/elgText";
import * as $ from "jquery";
import * as _ from "underscore";


function displayText(container: JQuery, text: ElgText, depth: number, annotationSelector: AnnotationSelector, viewport: JQuery = container, allowHTML : boolean = false) {
    let leafText: ElgText = undefined;
    text = flattenTokens(text, depth);
    if(text.content !== undefined) {
        leafText = text;
    } else if(text.texts) {
        // branch node
        const firstTextRole = roleFor(text.texts[0], depth);
        if(firstTextRole === "alternative") {
            displayTextsAsAlternatives(container, text.texts, depth, annotationSelector, viewport, allowHTML);
        } else {
            // segments
            const parentAnnotations = text.annotations;
            for(let i = 0; i < text.texts.length; i++) {
                let segment = text.texts[i];
                const segView = $('<div class="elg-segment"></div>').appendTo(container);
                const roleLabel = $('<span class="elg-segment-role"></span>').appendTo(segView);
                roleLabel.text(roleFor(segment, depth));
                if(parentAnnotations) {
                    segment = fillInParentAnnotations(segment, i, parentAnnotations);
                }
                // process this child, recursively
                displayText(segView, segment, depth + 1, annotationSelector, viewport, allowHTML);
            }
            // render any features at this level
            if (text.features && !$.isEmptyObject(text.features)) {
                // add features table
                container.append($('<h3>Features</h3>'));
                featureTable(text.features, container);
            }
        }
    } else {
        // nothing to show
    }

    if(leafText) {
        // leaf node
        const segment = new Segment(leafText.content, leafText.annotations, annotationSelector);
        const textDiv = $('<div></div>').appendTo(container);
        ColourFieldView.create(segment, textDiv, allowHTML);
        new Viewer(textDiv, viewport, segment);
        if (leafText.features && !$.isEmptyObject(leafText.features)) {
            // add features table
            container.append($('<h3>Features</h3>'));
            featureTable(leafText.features, container);
        }
    }
}

function displayTextsAsAlternatives(docView: JQuery, alternatives: Array<ElgText>, depth: number, annotationSelector: AnnotationSelector, viewport: JQuery = docView, allowHTML : boolean = false) {
    docView.empty();

    // special case - if fewer than two alternatives then no need to show buttons
    if (alternatives.length >= 2) {
        let curAlternative = 0;
        const anyAltHasScore = _.some(alternatives, (alt) => alt.hasOwnProperty('score'));
        const prevNextButtons = $(
            `<div class="elg-alternative-selector">
                <button class="mdc-button prev-alternative" disabled title="Previous alternative">
                    <i class="material-icons mdc-button__icon" aria-hidden="true">navigate_before</i>
                </button>
                <div class="elg-alternative-info">
                Alternative <span class="cur-alternative"></span>/${alternatives.length}
                ${anyAltHasScore ? '<div class="mdc-typography--caption">Score: <span class="cur-alternative-score"></span></div>' : ''}
                </div>
                <button class="mdc-button next-alternative" title="Next alternative">
                    <i class="material-icons mdc-button__icon" aria-hidden="true">navigate_next</i>
                </button>
            </div>`).appendTo(docView);

        const showCurrentAlt = () => {
            prevNextButtons.find('button.prev-alternative')
                .prop('disabled', (curAlternative == 0));
            prevNextButtons.find('button.next-alternative')
                .prop('disabled', (curAlternative == alternatives.length - 1));
            prevNextButtons.find('.cur-alternative').text(curAlternative + 1);
            prevNextButtons.find('.cur-alternative-score').text(
                alternatives[curAlternative].hasOwnProperty('score') ? alternatives[curAlternative].score : 'None');

            const alts = docView.children('div.elg-alternative');
            const toHide = alts.not(`.elg-alternative-${curAlternative}`);
            toHide.hide();
            const toShow = alts.filter(`.elg-alternative-${curAlternative}`);
            toShow.show();
        };
        prevNextButtons.on('click', 'button', function(e) {
            if ($(this).hasClass('prev-alternative')) {
                curAlternative--;
            } else {
                curAlternative++;
            }
            showCurrentAlt();
            return false;
        });
        
        // populate view with the right values for the first alternative
        showCurrentAlt();
    }

    for (let i = 0; i < alternatives.length; i++) {
        const alt = alternatives[i];
        const altView = $(`<div class="elg-alternative elg-alternative-${i}"></div>`);
        if (i > 0) {
            altView.hide();
        }
        altView.appendTo(docView);
        // but if fewer than two alternatives, need to show score here as it won't show between the buttons
        if (alternatives.length == 1 && alt.hasOwnProperty('score')) {
            $(`<div class="mdc-typography--caption">Score: ${alt.score}</div>`).appendTo(altView);
        }
        displayText(altView, alt, depth + 1, annotationSelector, viewport, allowHTML);
    }
}

/**
 * Given a set of annotations at a "branch" node in a texts response, merge any
 * annotations that cover this segment index as annotations over the whole span of
 * this segment.
 */
function fillInParentAnnotations(segment: ElgText, index: number, parentAnnotations: ElgAnnotations): ElgText {
    let newAnnots: ElgAnnotations;
    for (let [type, anns] of Object.entries(parentAnnotations)) {
        for (let ann of anns) {
            if(ann.start <= index && ann.end > index) {
                // this annotation covers this segment
                if(!newAnnots) {
                    newAnnots = {...segment.annotations};
                }
                if(newAnnots[type] === segment.annotations?.[type]) {
                    newAnnots[type] = [];
                    if(segment.annotations?.[type]) {
                        newAnnots[type].push(...segment.annotations?.[type]);
                    }
                }
                const newAnn: ElgAnnotation = {
                    sourceStart: ann.start,
                    sourceEnd: ann.end,
                    start: 0,
                    end: segment.texts?.length || segment.content?.length || 0,
                    features: {...ann.features},
                };
                if(segment.texts?.length) {
                    newAnn.end = segment.texts.length;
                } else {
                    newAnn.end = (segment.content ?? '').length;
                }
                newAnnots[type].unshift(newAnn);
            }
        }
    }

    if(newAnnots) {
        return {...segment, annotations: newAnnots};
    }
    return segment;
}

/**
 * Display a set of text segments or alternatives (depending on the role of each text).
 * @param texts texts to display
 * @param docView jQuery object referring to the main div for the docView
 * @param selector jQuery object referring to the div for the annotation selector
 * @param allowHTML should we render HTML in the content of a text, or escape it as text?
 */
export function displaySegmentsOrAlternatives(texts: Array<ElgText>, docView: JQuery, selector: JQuery, allowHTML : boolean = false) {
    ensureFeatureListener(docView);
    const annotationSelector = new AnnotationSelector();

    docView.empty();

    // build the display
    displayText(docView, {texts:texts}, 0, annotationSelector, docView, allowHTML);

    if(annotationSelector.knownTypes.next().done) {
        // no annotations
        selector.hide();
    } else {
        selector.show();
    }

    Selector.create(annotationSelector, selector);
}

/**
 * Display a single set of annotations (as returned by an IE service) with its associated selector.
 */
export function displayAnnotations(text: string, annotations: ElgAnnotations, docView: JQuery, selector: JQuery) {
    displaySegmentsOrAlternatives([{ content: text, annotations:annotations }], docView, selector);
}

export function featureTable(features: object|any[], parent: JQuery, includeHeader: boolean = true, startHidden: boolean = false): JQuery {
    const table = $(`<table class="featureTable"></table>`);
    if(startHidden) {
        table.hide();
    }
    table.appendTo(parent);
    const isArray = $.isArray(features);
    if(includeHeader) {
        const header = $(`<thead><tr>${ isArray ? '' : '<th>Name</th>'}<th colspan="2">Value</th></tr></thead>`);
        table.append(header);
    }
    const tbody = $('<tbody></tbody>').appendTo(table);

    const direction = table.closest('[dir]').prop('dir') || 'ltr';
    $.each(features, (featureName, featureValue) => {
        const tr = $(`<tr></tr>`).appendTo(tbody);
        if(!isArray) {
            $('<td></td>').text(featureName).appendTo(tr);
        }
        let valueCell = $('<td colspan="2"></td>').appendTo(tr);
        // dig down into single-element arrays like ["foo"], [["foo"]], etc.
        while($.isArray(featureValue) && featureValue.length == 1) {
            featureValue = featureValue[0];
        }
        if($.isPlainObject(featureValue)) {
            if($.isEmptyObject(featureValue)) {
                valueCell.html("<em>empty</em>");
            } else {
                const controlCell = valueCell;
                controlCell.data('elg-value', featureValue);
                controlCell.append($(`<a title="Expand" class="glyphicon glyphicon-chevron-${direction == 'rtl' ? 'left' : 'right'} elg-expand-feature"></a>`));
                controlCell.removeAttr("colspan");
                controlCell.addClass("elg-expand-control");

                valueCell = $('<td class="elg-expand-value"></td>').appendTo(tr);
                valueCell.append($(`<span class="elg-placeholder">{ &hellip; }</span>`))
            }
        } else if($.isArray(featureValue)) {
            if(featureValue.length == 0) {
                valueCell.html("<em>empty</em>");
            } else {
                const controlCell = valueCell;
                controlCell.data('elg-value', featureValue);
                controlCell.append($(`<a title="Expand" class="glyphicon glyphicon-chevron-${direction == 'rtl' ? 'left' : 'right'} elg-expand-feature"></a>`));
                controlCell.removeAttr("colspan");
                controlCell.addClass("elg-expand-control");

                valueCell = $('<td class="elg-expand-value"></td>').appendTo(tr);
                valueCell.append($(`<span class="elg-placeholder">[ &hellip; ]</span>`));
            }
        } else {
            try {
                const urlValue = new URL(featureValue);
                if(urlValue.protocol === "http:" || urlValue.protocol === "https:") {
                    const linkElt = $('<a target="_blank" class="elg-feature-value-url"></a>')
                        .attr('href', urlValue.href)
                        .appendTo(valueCell);
                    $('<span></span>').text(featureValue + " ").appendTo(linkElt);
                    $('<span class="glyphicon glyphicon-new-window"></span>').appendTo(linkElt);
                    return;
                }
            } catch(e) {
                // not a valid URL, fall through
            }
            valueCell.text(featureValue);
        }
    });

    return table;
}


function ensureFeatureListener(docView: JQuery) {
    if(!docView.data('elgFeatureListenerAdded')) {
        docView.on('click', 'a.elg-expand-feature', function(e) {
            e.stopPropagation();
            e.preventDefault();
            const link = $(this);
            const tr = link.closest('tr');
            const expandCell = tr.children('.elg-expand-control');
            const valueCell = tr.children('.elg-expand-value');
            let table = valueCell.children('.featureTable');
            if(table.length == 0) {
                // we haven't expanded this one yet, so construct the new table
                expandCell.append(`<a title="Collapse" class="glyphicon glyphicon-chevron-down elg-collapse-feature"></a>`);
                const features = expandCell.data('elg-value');
                table = featureTable(features, valueCell, false, true);
            }

            expandCell.children('.elg-expand-feature').hide();
            expandCell.children('.elg-collapse-feature').show();
            valueCell.children('.elg-placeholder').fadeOut("fast", () =>
                table.fadeIn(400));
        });

        docView.on('click', 'a.elg-collapse-feature', function(e) {
            e.stopPropagation();
            e.preventDefault();
            const link = $(this);
            const tr = link.closest('tr');
            const expandCell = tr.children('.elg-expand-control');
            const valueCell = tr.children('.elg-expand-value');

            expandCell.children('.elg-collapse-feature').hide();
            expandCell.children('.elg-expand-feature').show();
            valueCell.children('.featureTable').fadeOut("fast", () =>
                valueCell.children('.elg-placeholder').fadeIn(400));
        });
        docView.data('elgFeatureListenerAdded', true);
    }
}
