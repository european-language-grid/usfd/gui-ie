import "bootstrap"; // for popover
import * as $ from "jquery";
import {Segment} from "../data/segment";
import {Annotation} from "../data/annotation";
import {featureTable} from "../annotation-viewer";

export class Viewer {
    private segment : Segment;
    private target : JQuery;
    private popover : JQuery;

    constructor(target: JQuery, viewport: JQuery, segment: Segment) {
        this.segment = segment;
        this.target = target;

        const viewer = this;
        const viewportElement = viewport[0];

        this.target.popover({
            selector: "span[data-has-annotations=true]",
            html: true,
            placement: function(tip : HTMLElement, elt : HTMLElement) {
                // must add element to the tree in order to get its width
                $(tip).css({ top: 0, left: 0, display: 'block' }).insertAfter(elt);
                const viewportBox = viewportElement.getBoundingClientRect();
                const pos = this.getPosition();
                const actualWidth  = tip.offsetWidth;

                if(pos.right  + actualWidth  <= viewportBox.right) {
                    // will fit to the right
                    return "right";
                } else if(pos.left   - actualWidth  >= viewportBox.left) {
                    // will fit to the left
                    return "left";
                } else {
                    return "bottom";
                }
            },
            content: function() {
                // Generate the HTML to select an annotation
                return viewer.generatePopover(this);
            },
            viewport: viewport
        });

        // monkey-patch to stop adjusted delta going above top of the viewport
        const pop = $.fn.popover.Constructor.prototype;
        if(!pop.$editor_patched_getVAD) {
            const savedGetVAD = pop.getViewportAdjustedDelta;
            pop.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
                const delta = savedGetVAD.call(this, placement, pos, actualWidth, actualHeight);
                if (delta.top < 0) {
                    delta.top = 0;
                }
                return delta;
            };

            pop.$editor_patched_getVAD = true;
        }

        // Hide all other popovers
        this.target.on("click", "span[data-has-annotations]", (event) => {
            return viewport.find("span").not(event.currentTarget).filter(function(i, el) {
                return $(el).data('popover') || $(el).data('bs.popover'); // BS 2 or 3 compatibility
            }).popover("destroy");
        });

        // Hide all popovers on any change to annotation sets
        $(this.segment.annotationSelector).on('visible:changed', (event) => {
            return viewport.find(".popover").remove();
        });

    }

    public generatePopover(targetSpan : HTMLElement) {
        const offset : number = $(targetSpan).data('offset');
        // check if we've been given a selectedAnnot from the menu
        const selectedAnnot : number | undefined = $(targetSpan).data('selectedAnnot');

        this.popover = $(`<div class='tooltipContainer'><div class="content"></div></div>`);

        const annotations = this.segment.colourField.getAnnotations(offset);
        if(annotations.length == 1) {
            this.showAnnotation(annotations[0]);
        } else if(typeof(selectedAnnot) == 'number') {
            this.showAnnotation(annotations[selectedAnnot]);
        } else {
            this.showAnnotationMenu(annotations, targetSpan);
        }

        return this.popover[0];
    }

    public showAnnotationMenu(annotations : Array<Annotation>, targetSpan: HTMLElement) {
        const selector = $.parseHTML(`
        <div class='annotationSelector'>
            <h2>Please select an annotation to view</h2>
            <div class='annotationList list-group'></div>
        </div>
        `);

        const list = $(selector).find(".annotationList");

        // closure to bind the correct index for each handler
        const annotationClickHandler = (idx) => {
            return (event: JQueryEventObject) => {
                // hide the current popover, then re-show it with the selected annotation
                const ts = $(targetSpan);
                const reShow = () => {
                    // remove this listener again
                    ts.off('hidden.bs.popover', reShow);
                    ts.data('selectedAnnot', idx);
                    ts.popover('show');
                };
                ts.on('hidden.bs.popover', reShow);
                const clearData = () => {
                    ts.off('shown.bs.popover', clearData);
                    ts.removeData('selectedAnnot');
                };
                ts.on('shown.bs.popover', clearData);
                setTimeout(() => {
                    ts.popover('hide');
                }, 0);
                return false;
            }
        };

        for (let i = 0; i < annotations.length; i++) {
            const annotation = annotations[i];
            const annotationDOM = $(`<a href='#' class="list-group-item">${annotation.type}</a>`);
            annotationDOM.on("click", annotation, annotationClickHandler(i));
            list.append(annotationDOM);
        }

        $(this.popover).children().replaceWith(list);
    }

    public showAnnotation(annotation : Annotation) {
        const result = $(`<form class='annotationEditor'></form>`);

        result.append($('<div></div>').append($('<strong></strong>').text(annotation.type)));
        if(annotation.sourceStart || annotation.sourceEnd) {
            result.append($('<div></div>')
                .text(`Source: ${annotation.sourceStart || 'start'}-${annotation.sourceEnd || 'end'}`))
        }

        // create the features table
        if (annotation.features && !$.isEmptyObject(annotation.features)) {
            featureTable(annotation.features, result);
        }
        else {
            result.append($(`
	            <div>
	                <i>No Features</i>
	            </div>`));
        }

        $(this.popover).children().replaceWith(result);
    }

}