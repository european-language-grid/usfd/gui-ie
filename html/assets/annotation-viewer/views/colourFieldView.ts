import * as _ from "underscore";
import * as $ from "jquery";

import {Segment} from "../data/segment";
import {Span} from "../data/span";
import {Annotation} from "../data/annotation";
import {Colour} from "../data/colour";
import {ColourField} from "../data/colourField";


export class ColourFieldView {
    private colourField : ColourField;
    private viewer : JQuery;
    private spans : Array<Span>;
    private text;
    private allowHTML: boolean;

    /*
      Encapsulates the generation of the required DOM objects to show overlapping annotations
    */
    constructor(text : string, colourField : ColourField, target : JQuery, allowHTML : boolean = false) {
        // Construct a bitmap representing each character with a different colour
        this.colourField = colourField;
        this.viewer = target;
        this.spans = new Array<Span>();
        this.text = text;
        this.allowHTML = allowHTML;
        $(this.colourField).on("changed", (event, data) => {
            if (data) {
                this.invalidate(data.start, data.end);
            } else {
                this.updateAnnotations();
            }
        });
    }

    public static create(segment : Segment, target:JQuery, allowHTML : boolean = false) : ColourFieldView {
        const result = new ColourFieldView(segment.text, segment.colourField, target, allowHTML);
        result.updateAnnotations();
        return result;
    }

    public updateAnnotations() {
        // Just regenerate everything
        this.spans = this.generateSpansInRange();
        this.renderSpans(this.spans);
        return this.update();
    }

    public invalidate(left : number = null, right : number = null) {
        let new_spans: Span[], old_spans: Span[], _ref: number;
        //this.colourField.update();
        
        if (left == null || right == null) {
            return this.updateAnnotations();
        }
        // Find our spans that the annotation covers
        const span_range = this.findSpanRange(left, right);

        // REnder spans in the range that we're about to remove.
        // This is not the same as the range covered by the annotation, as the annotation may be
        // adjacent to others of the same colour
        new_spans = this.renderSpans(this.generateSpansInRange(this.spans[span_range[0]].start, this.spans[span_range[1]].end));

        old_spans = this.spans.slice(span_range[0], +span_range[1] + 1 || 9e9);

        // Replace the spans in our internal datastructure
        [].splice.apply(this.spans, ([(_ref = span_range[0]), span_range[1] - _ref + 1] as any[]).concat(new_spans));

        // And replace them in the DOM
        const new_span_nodes = new_spans.map((span) => span.node);
        const old_span_nodes = old_spans.map((span) => span.node);

        $(old_span_nodes).first().before(new_span_nodes);
        return $(old_span_nodes).remove();
    }

    /**
     * Discovers each contiguous colour block in range and builds a span for it.
     * @param start Where to start
     * @param end Where to end
     * @returns {Array<Span>}
     */
    public generateSpansInRange(start : number = null, end : number = null) : Array<Span> {
        let annotations : Array<Annotation>, colour : Colour, lastAnnotations : Array<Annotation>,
            lastColour : Colour, lastOffset : number;
        const spans : Array<Span> = [];
        start = start === null ? 0 : start;
        end = end === null ? this.text.length - 1 : end;

        lastOffset = start;
        lastColour = this.colourField.get(lastOffset);
        lastAnnotations = this.colourField.getAnnotations(lastOffset);
        for (let i = start; i <= end; i++) {
            colour = this.colourField.get(i);
            annotations = this.colourField.getAnnotations(i);
            if (!_.isEqual(lastAnnotations, annotations)) {
                spans.push(new Span(lastOffset, i - 1, lastColour, lastAnnotations));

                lastColour = colour;
                lastOffset = i;
                lastAnnotations = annotations;
            }
        }
        spans.push(new Span(lastOffset, end, lastColour, lastAnnotations));

        return spans;
    }

    /**
     * Finds spans that are touching the given range. Useful for updating without updating everything.
     * @param start
     * @param end
     * @returns {any}
     */
    public findSpanRange(start : number = null, end : number = null) : number[] {
        let left, mid, right;
        if (this.spans === []) {
            return [];
        }
        if (start == null || end == null) {
            return [0, this.spans.length - 1];
        }

        // First find the first node.
        left = 0;
        right = this.spans.length - 1;
        mid = Math.floor((left + right) / 2);
        while (!(this.spans[mid].start >= start && this.spans[mid].start <= end) && left <= right && mid > -1) {
            if (this.spans[mid].start < start) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
            mid = Math.floor((left + right) / 2);
        }


        // Now search left until we see something that doesn't work.
        left = mid;
        right = mid;
        while ((left > 0) && (this.spans[left - 1].end >= start)) {
            left -= 1;
        }

        while ((right < this.spans.length - 1) && (this.spans[right + 1].start <= end)) {
            right += 1;
        }

        return [left, right];
    }

    public addNewLines(text) {
        return text.split("\n").join("<br>");
    }

    /**
     * Returns the specified spans as nodes in the document.
     * @param spans Which spans to place. We don't assume the whole document wants updating as this would be expensive.
     * @returns {any}
     */
    public renderSpans(spans : Span[]): Span[]  {
        let result = [];
        spans.forEach((span) => {
            // Construct a node for the annotation.
            const text : string = this.text.slice(span.start, span.end + 1);

            const hasAnnotations = !$.isEmptyObject(span.annotations);
            const colour = span.colour;
            const spanNode = $("<span data-has-annotations='" + hasAnnotations + "' data-offset='" + span.start + "'>");
            if(this.allowHTML) {
                // we allow HTML, so insert <br> tags into the text and then parse that
                spanNode.append($.parseHTML(this.addNewLines(text)));
            } else {
                // we don't allow HTML, so split into lines, make a plain text node
                // from each line, and insert <br> between lines.
                const lines = text.split("\n");
                for(let j = 0; j < lines.length; j++) {
                    if(j > 0) {
                        spanNode.append(document.createElement('br'));
                    }
                    spanNode.append(document.createTextNode(lines[j]));
                }
            }
            if (hasAnnotations) {
                spanNode.css("background-color", colour.rgbString);
            }
            span.node = spanNode.get(0);
            this.attachEvents(span);
            return result.push(span);
        });

        return result;
    }

    public attachEvents(span) {
        return $(span.node).on("mouseover", null, span, (event) => this.showSpan(event.data));
    }

    public showSpan(span) {
        let text = "";
        const _ref = span.annotations;
        for (let annotationId in _ref) {
            const annotation = _ref[annotationId];
            text += annotation.type + "<br />";
        }
        return $("#annotationInfo").html(text);
    }

    public update() {
        const nodes = this.spans.map((span) => span.node);
        this.viewer.empty();
        return this.viewer.append(nodes);
    }

}
