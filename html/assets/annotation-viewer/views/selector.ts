///<reference path="../../lib/jquery.d.ts"/>
///<reference path="../../lib/bootstrap.d.ts"/>

import * as $ from "jquery";
import {Colour} from "../data/colour";
import {AnnotationSelector} from "../data/annotationSelector";

export class Selector {
    /* Shows a control to allow the user to select which annotation sets are displayed*/
    private target : JQuery;
    private annotationSelector : AnnotationSelector;

    constructor(annotationSelector : AnnotationSelector, target : JQuery) {
        this.annotationSelector = annotationSelector;
        this.target = target;
    }

    public static create(annotationSelector : AnnotationSelector, target : JQuery) : Selector {
        const sel = new Selector(annotationSelector, target);
        sel.update();
        sel.updateSelection();
        return sel;
    }

    public update() {
        const annotationSelections = $("<form></form>");
        
        // need to avoid the use of this as we are going to use this in a function
        const annotationSelector = this.annotationSelector;
        
        const annotationTypeNames = Array.from(annotationSelector.knownTypes).sort();

        annotationTypeNames.forEach(function(annotationType: string) {
            const colour : Colour = annotationSelector.typeColour(annotationType, 1);

            //noinspection CssInvalidPropertyValue
            const entry = $.parseHTML(`<label style='color: ${colour.rgbString}'>
                <input type='checkbox' name='annotations' value='${annotationType}'>
                ${annotationType}</label>`);

            // Select this entry in the list if it's supposed to already be visible.
            if (annotationSelector.isAnnotationVisible(annotationType)) {
                $(entry).find("input:checkbox").attr("checked", "checked");
            }
            $(annotationSelections).append(entry);
        });
        $(annotationSelections).find("input:checkbox").on("change", this.updateSelection.bind(this));
        this.target.empty();
        this.target.append($('<h3>Annotations</h3>'));
        return this.target.append(annotationSelections);
    }

    public updateSelection() {
        const annotationTypes : string[] = [];
        $(this.target).find("input:checked").map(function() : string {
            return $(this).val();
        }).each((index, selection : any) => {
            annotationTypes.push(selection); // Converts to a tuple
        });

        this.annotationSelector.visibleTypes(annotationTypes);
    }


}

