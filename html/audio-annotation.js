require(["jquery", "material-components-web", "elg/common", "elg/audio-input", "elg/audio-annotations", "elg/binary-samples"], function ($, mdc, ElgCommon, audioInput, audioAnnotations, binarySamples) {
    $(function () {
        mdc.autoInit();

        function enableRecord() {
            $('#recordmic, #selectfile').prop('disabled', false);
        }

        function enableSubmit() {
            $('#submit-form').prop('disabled', false);
        }

        var elgCommon = new ElgCommon(enableRecord, enableSubmit,
            document.getElementById('submitprogress').MDCLinearProgress,
            binarySamples($('#samples-list'), $('#samples-container'), "audio", audioInput.audioReady));

        var handleResponse = audioAnnotations.responseHandler(elgCommon);

        audioInput.setup(elgCommon, handleResponse);
    });
});


