# GUI for "QA-in-context" services

`index-qa-in-context.html` is a GUI for services that expect _two_ pieces of text as their input rather than one.  It was primarily designed for services that do question answering from context, given a short question text along with a longer "context" text that may contain an answer to the question.  The service input may be in one of two formats:

- a "text" request with the context in the main request context and the question provided as a parameter, or vice versa:
  ```json
  {
    "type": "text",
    "content": "This is the context that may answer a question",
    "params": {
      "q": "This is the question"
    }
  }
  ```
- a "structuredText" request with two segments, one for the context and one for the question, either way around:
  ```json
  {
    "type": "structuredText",
    "texts": [
      {"content": "This is the context"},
      {"content": "This is the question"}
    ]
  }
  ```
  
The request format is configured by the ELG service validator using query parameters on the `index-qa-in-context.html` "try out" GUI URL.

- `format` to select one of the two basic forms above.  `format=param` means the "text" request format with a parameter, `format=structured` (the default) means the two-segment "structuredText" format
- `context` and/or `question` define which part is the context and which is the question.
  - for `format=param` specify _either_ `context` _or_ `question` but not both, with the value being the name of the parameter containing the relevant item.  If you specify `question` that means the question is a parameter and the context is the main request content, if you specify `context` then the context is a parameter and the question is the main content.
  - for `format=structured` specify _both_ `context` and `question` with numeric values `0` and `1`.  Setting `context=0&question=1` means the context will be the first item in the "structuredText" request and the question will be second, setting `question=0&context=1` means they will be the other way around with the question first and context second.
- `context-name` and `question-name` (default "Context" and "Question" respectively) give the placeholder text used to label the two input fields.  These are configurable to enable this same GUI to be used for other two-texts-input tasks that are not strictly related to question answering.
- `context-desc` and `question-desc` allow overriding of the help message that appears below each of the two input fields when they have the input focus.  Again, the defaults are suitable for QA-in-context services but are configurable for other two-texts tasks.

This GUI also supports many of the standard parameters defining the interpretation of the response, including `dir` (text direction of the question and answer), `roles`, and the `show` and `hide` parameters, if the response contains any annotations.

## Sample data

This GUI supports sample data in the service metadata, which is assumed to be JSON in whatever format the service expects to receive.  Each JSON sample will be unpacked into the two text inputs by following the `format`/`context`/`question` parameters described above and presented as clickable items in a list, which populate the two input fields when clicked.

## Supported response formats

This GUI can support service responses in any of three formats.

- `texts` responses will be rendered exactly as per the standard `index.html` with annotations if any are included.  This will probably be the most common format for question answering services, with the response including the text of the answer.
- `annotations` responses are assumed to be annotations relative to the _context_ text from the request, e.g. for services that return the location of the answer within the context rather than just the plain answer on its own
- `classification` responses are rendered as a table exactly as in `index-text-classification.html`.  This format is less likely to be useful for QA services specifically but may be used by other service types that compare two texts, e.g. classifying the stance (support vs deny) of a reply text with respect to the original text to which it is replying, or determining whether two news texts refer to the same event.
