# GUI for dependency parsers

`index-dependency.html` is a GUI designed for dependency parser services.  It can work with services that return parse results in a variety of formats, as long as they follow this basic structure:

- one or more sentences
- each consisting of one or more words
- each with a feature linking this word to its parent in the dependency graph
- and optionally with additional features giving other data such as a lemma, dependency relation type, POS tag, etc.

The exact detail of how this structure is implemented for a particular service is controlled by a set of query parameters on the `/dev/gui-ie/index-dependency.html?...` query string. 

## Writing direction

`index-dependency.html` supports the standard `dir=ltr` or `dir=rtl` parameter to specify the writing direction of the language.  This controls the directionality of the input box and the rendered string in the result view, however the tree itself is always rendered left-to-right.

## Sentences, tokens and words

The GUI can handle several formats for sentences and tokens:

### Standoff annotations

An `annotations` response type with one annotation type for the tokens and another for the sentences, where each sentence annotation spans over one or more tokens:

```json
{
  "response": {
    "type": "annotations",
    "annotations": {
      "Sentence": [
        {"start": 0, "end": 10}
      ],
      "Token": [
        {"start": 0, "end": 2, "features": {"id": "tok1"}},
        {"start": 3, "end": 5, "features": {"id": "tok2", "parent": "tok1"}}
      ]
    }
  }
}
```

For this format specify parameters

- `sentence` - the annotation type representing sentences
- `token` - the annotation type representing tokens

In the example above `sentence=Sentence&token=Token`.  The text of each token is by default the string under the token annotation, but this can be overridden by specifying `form=<feature-name>` in which case the word form will be taken from the specified feature.

### List of sentences, with standoff tokens

A `texts` response where each leaf node (one with `content` rather than another level of `texts`) represents a sentence, and within that sentence the tokens are standoff annotations as above:

```json
{
  "response": {
    "type": "texts",
    "texts": [
      {
        "content": "This is the first sentence.",
        "annotations": {
          "Token": [
            {"start": 0, "end": 4, "features": {"id": "tok1", "parent": "tok2"}},
            {"start": 5, "end": 7, "features": {"id": "tok2"}}
          ]
        }
      },
      {"_":"etc. for subsequent sentences..."}
    ]
  }
}
```

For this style specify just the `token` parameter without the `sentence` one - if `sentence` is unset each leaf node will be treated as one sentence.  As in the pure standoff case the text of each token is taken from the string covered by the annotation, but can be taken from a feature instead using `form=<feature-name>`.

### List of sentences, list of tokens

A `texts` response where the leaf nodes are tokens and the next level up are the sentences:

```json
{
  "response": {
    "type": "texts",
    "texts": [
      {
        "texts": [
          {"content": "This", "features": {"id": "tok1", "parent": "tok2"}},
          {"content": "is", "features": {"id": "tok2"}}
        ]
      }
    ]
  }
}
```

For this format specify `roles=sentence-word&token=word`:

- `roles=sentence-word` - sets the default role of texts at each level, so first level is sentences, second level is words.  As described in [the README](README.md#configuration) a role of "word" or "token" is handled specially, and a branch node containing a list of texts with this role is flattened into a leaf node with the children as word/token annotations (named after the role)
- `token=word` - takes these synthesized "word" annotations and uses them as the token annotation type [as above](#list-of-sentences-with-standoff-tokens).

By default the text of each word is taken from its `content`, but you can specify `form=<feature-name>` to take it from a feature instead.

### Multi-word tokens

Some parsers produce a third level of output, where one token can be made up of multiple logical _words_:

```json
{
  "response": {
    "type": "texts",
    "texts": [
      {
        "content": "Supercapacitors are...",
        "annotations": {
          "Token": [
            {"start": 0, "end": 15, "features": {"words": [
              {"str": "Super", "id": "w1", "parent": "w2"},
              {"str": "capacitors", "id": "w2", "parent": "w3"}
            ]}},
            {"start": 16, "end": 19, "features": {"words": [
              {"str": "are", "id": "w3"}
            ]}}
          ]
        }
      }
    ]
  }
}
```

For parsers that do this, set `?words=<feature-name>` (e.g. `words=words` in this example) to name a feature of each token that contains the list of words.  All other features described below (the ID, parent links, etc.) will be taken from the word object rather than the token annotation features.  If a parser return multiple words per token you will almost certainly want to set the `form=...` parameter to take the text for each word from a feature, otherwise all words will show with the text of the _whole_ token.

If `words` is not set, each token is treated as a single word.

## Dependency links

Once the list of sentences and words has been determined, we use features from each token to link the nodes into a dependency tree.  Each token links to at most one parent node in the tree via a feature named by the `?parent=...` parameter (which defaults to "parent" if not specified), nodes with no parent link are linked to an implicit `<root>` node at the top of the tree for that sentence, as are nodes whose parent link is invalid (referring to a non-existent node, or a link that would create a cycle in the dependency graph).  The parent link can be:

- an explicit identifier, which will look up based on an identifier feature on the target node (by default "id", but can be overridden with `?id=<feature-name>`), or
- a number (either a bare number or a quoted number-as-a-string), taken as an index into the list of tokens for this sentence.
  - By default the first token is considered number 1, and a parent link of `0` is treated the same as a missing or null link, linking to the implicit root node.
  - If you set `first=0` then the first token is considered number zero, and negative numbers link to the implicit root node.

## Node labels

By default the nodes in the tree are labelled with the text of the word (either the text covered by the annotation or the nominated `form` feature).  This can be overridden by specifying one or more `label` parameters.  Each label parameter can be either `text` (to use the text of the word) or the name of any other word feature (to use that feature value converted to a string), and may optionally be preceded by a six-digit hexadecimal colour code and semicolon, to render that label in the given colour.  Multiple labels are rendered one above the next.  For example:

```
label=text&label=00ff00;pos
```

would create a two-line label on each node, the text of the word (in black) above and the `pos` feature value below in bright green.

## Presets

Since there are so many parameters, the GUI provides presets for common groups of services which can be enabled using `?preset=<name>`.  Currently supported presets are:

- `udpipe` (for Charles University UDPipe parsers)
  - `token=udpipe/tokens`, `sentence=udpipe/sentences`
  - `words=words`, `form=form` (each token has a list of words that each has the word form in a "form" feature)
  - `parent=head`, using one-based token indexes - no explicit word identifiers
  - Each node labelled with the word form, deprel and upos features.
