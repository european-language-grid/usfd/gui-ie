FROM --platform=$BUILDPLATFORM node:20-slim AS builder
WORKDIR /home/node/app
COPY package.json ./
COPY package-lock.json ./
RUN npm ci

COPY html ./html/
RUN npm run build

FROM nginx:alpine
COPY nginx/default.conf /etc/nginx/conf.d/
COPY --from=builder /home/node/app/html /usr/share/nginx/html
